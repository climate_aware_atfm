# Climate - aware ATFM

#### Céline Demouge

This code is linked to the paper: Climate-aware air traffic flow management optimization via column generation, Demouge et al. Please refer to it before reading or launching this software.

The 2D repository contains code for 2D tests and 3D for 3D tests. 
Data repository and input_computation can be used for instances building. 

Please refer any question to celine.demouge@enac.fr.
