package optimization;

import java.util.ArrayList;
import java.util.Arrays;
import aircraft.Flight;
import config.Config;
import geometry.Arc;
import geometry.Graph;
import geometry.Node;
import geometry.Sector;
import weather.WindData;
import weather.Contrailsdata;


/**
 * Subproblem class.
 */
public class SubProblem {

    public static final double ACCF_CO2 = 6.94 * Math.pow(10, -5);

    /**
     * Solving the subproblem considered with the different parameters.
     * 
     * @param aircraft
     * @param lambdas
     * @param nT
     * @param nS
     * @param graph
     * @param wd
     * @param ad
     * @param p
     * @return
     */
    public static SubProblemResult solvingSubProblem(Flight aircraft, double[] lambdas, int nT, int nS, Graph graph,
            WindData wd, Contrailsdata ad, Config p) {

        int indexTDep = aircraft.getIndexTDep();

        int maxT = aircraft.getTmax();
        double INF = p.getINF();

        double INF2 = p.getINF_2();
        double DT = p.getDT();
        int nPoints = graph.getPoints().size();

        double[] costsSP_ = new double[nPoints * nT];

        double[] costs_ = new double[nPoints * nT];

        double[] masses_ = new double[nPoints * nT];

        int[] predecessorsP_ = new int[nPoints * nT];
        int[] predecessorsT_ = new int[nPoints * nT];

        // Costs initialization

        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < nT; j++) {
                if (graph.getPoints().get(i).getId().equals(aircraft.getPointDepId())) {
                    if (j == indexTDep) {

                        costsSP_[j * nPoints + i] = 0;
                        costs_[j * nPoints + i] = 0;

                        masses_[j * nPoints + i] = aircraft.getMass();

                    } else {

                        costsSP_[i + nPoints * j] = INF;
                        costs_[i + nPoints * j] = INF;

                        masses_[j * nPoints + i] = INF;

                    }
                } else {

                    costsSP_[i + nPoints * j] = INF;
                    costs_[i + nPoints * j] = INF;

                    masses_[j * nPoints + i] = INF;

                }
            }
        }

        int t = indexTDep;

        double alpha = p.getAlpha();

        ArrayList<Sector> sectorGraph = graph.getSectors();

        // Dynamic programming process
        while (t < maxT) {

            for (int i = 0; i < nPoints; i++) {

                Node p1 = graph.getPoints().get(i);

                // If the iteration is not useful, then continue
                if (aircraft.isProhibited(p1)) {
                    continue;
                }

                if (costs_[t * nPoints + i] > INF2) {
                    continue;
                }

                for (Arc a : graph.getNeighborsArcs(p1)) {
                    Node p2 = a.getP2();
                    if (aircraft.isProhibited(p2)) {
                        continue;
                    }
                    int j = graph.getIndex(p2);

                    double tArc = computeDt(aircraft, a, wd, DT);

                    int dt = (int) ((int) tArc / DT);

                    double m = masses_[t * nPoints + i];

                    if ((t + dt < maxT) && (m < INF)) {

                        int m_ = (int) (m / 10);

                        double ff = aircraft.getEmissions().getFuelFlow(m_);
                        double tA60 = tArc * 60.;
                        double[] cs = costArcSP(aircraft, a, wd, ad, lambdas, t, dt, sectorGraph, m_, tArc,
                                p.getCost(), ff, tA60, nT, alpha);
                        double c = cs[1];

                        if (costsSP_[(t + dt) * nPoints + j] > costsSP_[i + t * nPoints] + c) {
                            costsSP_[(t + dt) * nPoints + j] = costsSP_[i + t * nPoints] + c;

                            predecessorsP_[j + (t + dt) * nPoints] = i;
                            predecessorsT_[j + (t + dt) * nPoints] = t;

                            masses_[(t + dt) * nPoints + j] = masses_[(t) * nPoints + i]
                                    - ff * tA60;
                            costs_[(t + dt) * nPoints + j] = costs_[(t) * nPoints + i] + cs[0];

                        }
                    }

                }

            }

            t += 1;

        }

        // Path and solution recovery
        ArrayList<Node> pathInversed = new ArrayList<>();
        Node pArr = aircraft.getPointArr();
        pathInversed.add(pArr);
        int indexP = pArr.getIndex();

        int tP = argmin(costsSP_, nPoints, nT, indexP, INF, 1, indexTDep, maxT + 1, nT, nPoints);

        if (tP == -1) {
            System.out.println("FAILURE");
            SubProblemResult result = new SubProblemResult(Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                    null, null, Double.MAX_VALUE, Double.MAX_VALUE);

            return result;
        }

        int tEnd = tP;
        int indexPEnd = indexP;

        int[] sectorsCrossed = new int[nT];

        for (int tt = 0; tt < nT; tt += 1) {
            sectorsCrossed[tt] = -1;
        }

        boolean finished = false;

        while (!finished) {

            int indexPOld = indexP;
            int tOld = tP;

            indexP = predecessorsP_[indexPOld + nPoints * tOld];
            tP = predecessorsT_[indexPOld + nPoints * tOld];
            Node pP = graph.getPoints().get(indexP);

            Arc a = graph.fromNodes(pP, graph.getPoints().get(indexPOld));

            ArrayList<Integer> sectorsCrossedP = a.getSectorsCrossed(graph.getSectors(), tOld - tP);

            for (int tt = 0; tt < tOld - tP; tt += 1) {
                if (tt < sectorsCrossedP.size()) {
                    sectorsCrossed[tP + tt] = sectorsCrossedP.get(tt);

                }

            }

            finished = pP.getId().equals(aircraft.getPointDepId());

            pathInversed.add(pP);

        }

        SubProblemResult result = new SubProblemResult(costsSP_[indexPEnd + tEnd * nPoints],
                costs_[indexPEnd + nPoints * tEnd],
                0., 0., 0.,
                sectorsCrossed, pathInversed, 0., 0.);

        boolean ok = false;
        int k = 2;
        // Iterating on solutions if the first ones are in tabu list (avoiding loops)
        while (!ok) {

            if (aircraft.inTabu(result.getS())) {
                pathInversed = new ArrayList<>();
                pathInversed.add(aircraft.getPointArr());
                indexP = graph.getIndex(aircraft.getPointArr());
                tP = argmin(costsSP_, nPoints, nT, indexP, INF, k, indexTDep, maxT + 1, nT, nPoints);
                if (tP == -1) {
                    System.out.println("FAILURE");
                    return new SubProblemResult(Double.MAX_VALUE, Double.MAX_VALUE,
                            Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                            null, null, Double.MAX_VALUE, Double.MAX_VALUE);

                }

                tEnd = tP;
                indexPEnd = indexP;

                sectorsCrossed = new int[nT];
                for (int tt = 0; tt < nT; tt += 1) {
                    sectorsCrossed[tt] = -1;
                }

                finished = false;

                while (!finished) {

                    int indexPOld = indexP;
                    int tOld = tP;

                    indexP = predecessorsP_[indexPOld + nPoints * tOld];
                    tP = predecessorsT_[indexPOld + nPoints * tOld];
                    if (tP < aircraft.getIndexTDep()) {
                        System.out.println("FAILURE LOOP " + aircraft.getId());
                        return new SubProblemResult(Double.MAX_VALUE, Double.MAX_VALUE,
                                Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                                null, null, Double.MAX_VALUE, Double.MAX_VALUE);
                    }
                    Node pP = graph.getPoints().get(indexP);
                    Arc a = graph.fromNodes(pP, graph.getPoints().get(indexPOld));

                    ArrayList<Integer> sectorsCrossedP;
                    if (a == null) {
                        sectorsCrossedP = Arc.getSectorsCrossed(pP, graph.getPoints().get(indexPOld),
                                graph.getSectors(), tOld - tP);
                    } else {
                        sectorsCrossedP = a.getSectorsCrossed(graph.getSectors(), tOld - tP);
                    }

                    for (int tt = 0; tt < tOld - tP; tt += 1) {
                        if (tt < sectorsCrossedP.size()) {
                            sectorsCrossed[tP + tt] = sectorsCrossedP.get(tt);

                        }

                    }

                    finished = pP.getId().equals(aircraft.getPointDepId());

                    pathInversed.add(pP);
                    result = new SubProblemResult(costsSP_[indexPEnd + tEnd * nPoints],
                            costs_[indexPEnd + nPoints * tEnd],
                            0., 0., 0.,
                            sectorsCrossed, pathInversed, 0., 0.);
                    k += 1;
                }

            } else {
                break;
            }
            if (k > 50) {
                // If it goes to a too high value of k, then there is no new variable
                return new SubProblemResult(INF, INF,
                        0., 0., 0.,
                        null, null, 0., 0.);
            }
        }

        return result;
    }

    /**
     * Cost of the arc
     * 
     * @param aircraft
     * @param arc
     * @param wd
     * @param ad
     * @param dt
     * @param mass
     * @param time
     * @param costC
     * @param ff
     * @param tA60
     * @param alpha
     * @return
     */
    public static double costArc(Flight aircraft, Arc arc, WindData wd, Contrailsdata ad, int dt,
            int mass, double time, String costC, double ff, double tA60, double alpha) {

        if (costC.equals("accf")) {

            return ACCF_CO2 * ff * tA60
                    + arc.getACCF();
        }
        if (costC.equals("gwp20")) {

            return ff * tA60
                    * (1. + 2.2 * arc.getPourcent());
        }
        if (costC.equals("gwp100")) {

            return ff * tA60
                    * (1. + 0.63 * arc.getPourcent());
        }
        if (costC.equals("multi")) {
            return (1. - alpha) * ff * tA60 + alpha * tA60 * arc.getPourcent();
        }

        return ff * tA60;
    }

    /**
     * Cost of the arc in the subproblem (with lambdas)
     * 
     * @param aircraft
     * @param arc
     * @param wd
     * @param ad
     * @param lambdas
     * @param t
     * @param dt
     * @param sectors
     * @param mass
     * @param time
     * @param costC
     * @param ff
     * @param tA60
     * @param nT
     * @param alpha
     * @return
     */
    public static double[] costArcSP(Flight aircraft, Arc arc, WindData wd, Contrailsdata ad,
            double[] lambdas, int t, int dt, ArrayList<Sector> sectors, int mass, double time, String costC, double ff,
            double tA60, int nT, double alpha) {

        double s = 0;
        int t_index = t;

        ArrayList<Integer> sectorsCross = arc.getSectorsCrossed(sectors, dt);

        for (int i = 0; i < sectorsCross.size(); i++) {

            s += lambdas[t_index + sectorsCross.get(i) * nT];
            t_index += 1;

        }
        double[] results = new double[2];
        results[0] = costArc(aircraft, arc, wd, ad, dt, mass, time, costC, ff, tA60, alpha);
        results[1] = results[0] - s;

        return results;
    }

    /**
     * @param aircraft
     * @param arc
     * @param wd
     * @param DT
     * @return
     */
    public static double computeDt(Flight aircraft, Arc arc, WindData wd, double DT) {
        return arc.getTime(aircraft.getAirspeed());

    }

    /**
     * @param aircraft
     * @param p0
     * @param p1
     * @param wd
     * @param DT
     * @return
     */
    public static double computeDt(Flight aircraft, Node p0, Node p1, WindData wd, double DT) {
        return p0.time(p1, aircraft.getAirspeed(), wd) / DT;
    }

    /**
     * Get argmin for solution recovery.
     * 
     * @param tab
     * @param n
     * @param m
     * @param line
     * @param INF
     * @return
     */
    public static int argmin(double[][] tab, int n, int m, int line, double INF) {
        int index = -1;
        double value = INF;
        for (int i = 0; i < m; i++) {

            if (tab[line][i] < value) {
                value = tab[line][i];
                index = i;
            }
        }
        return index;
    }

    /**
     * Get index of the kth smallest value
     * 
     * @param tab
     * @param n
     * @param m
     * @param line
     * @param INF
     * @param k
     * @param tmin
     * @param tmax
     * @param nT
     * @param nPoints
     * @return
     */
    public static int argmin(double[] tab, int n, int m, int line, double INF, int k, int tmin, int tmax, int nT,
            int nPoints) {
        double[] newTab = new double[nT];
        for (int i = 0; i < nT; i++) {
            newTab[i] = tab[i * nPoints + line];
        }
        return getIndexOfIthSmallestValue(newTab, k, tmin, tmax);
    }

    public static int getIndexOfIthSmallestValue(double[] array, int i, int tmin, int tmax) {
        if (array == null || array.length == 0 || i <= 0 || i > array.length) {
            throw new IllegalArgumentException("Invalid input");
        }

        double[] sortedArray = Arrays.copyOfRange(array, 0, array.length);

        Arrays.sort(sortedArray);

        return indexOfValue(array, sortedArray[i - 1]);
    }

    private static int indexOfValue(double[] array, double value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return -1;
    }

}
