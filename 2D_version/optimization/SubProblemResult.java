package optimization;

import java.util.ArrayList;
import java.util.Arrays;


import geometry.Node;

/**
 * Class representing result of a subproblem. 
 */
public class SubProblemResult {

    private double costSP;
    private double cost;
    private double emissionsCO2;
    private double time;
    private double distance;
    private int[] sectorsCrossed;
    private ArrayList<Node> path;
    private double timeInContrails;
    private double distanceInContrails;
    /**
     * String used for tabu (id of the path)
     */
    private String s;



    /**
     * Constructor.
     * @param costSP
     * @param cost
     * @param emissionsCO2
     * @param time
     * @param distance
     * @param sectorsCrossed
     * @param pathInversed
     * @param timeInContrails
     * @param distanceInContrails
     */
    public SubProblemResult(double costSP, double cost, double emissionsCO2, double time, double distance,
            int[] sectorsCrossed, ArrayList<Node> pathInversed,double timeInContrails,double distanceInContrails) {
        this.costSP = costSP;
        this.cost = cost;
        this.emissionsCO2 = emissionsCO2;
        this.time = time;
        this.distance = distance;
        this.sectorsCrossed = sectorsCrossed;
        this.distanceInContrails=distanceInContrails;
        this.timeInContrails=timeInContrails;
        this.path = new ArrayList<>();
        this.s = "";

        if (pathInversed!=null){
            for (int i=1;i<=pathInversed.size(); i++){
                this.path.add(pathInversed.get(pathInversed.size()-i));
            }
            for (Node n:this.path){
                s+=n.getId();
            }
        }
        
        

    }

    

    public double getCostSP() {
        return costSP;
    }

    public double getCost() {
        return cost;
    }

    public double getEmissionsCO2() {
        return emissionsCO2;
    }

    public double getTime() {
        return time;
    }

    public double getDistance() {
        return distance;
    }

    public int[] getSectorsCrossed() {
        return sectorsCrossed;
    }

    

    public double getTimeInContrails() {
        return timeInContrails;
    }

    public double getDistanceInContrails() {
        return distanceInContrails;
    }

    

    @Override
    public String toString() {
        return "SubProblemResult [costSP=" + costSP + ", cost=" + cost + ", emissionsCO2=" + emissionsCO2 + ", time="
                + time + ", distance=" + distance + ", sectorsCrossed=" + Arrays.toString(sectorsCrossed) + ", path="
                + path + ", timeInContrails=" + timeInContrails + ", distanceInContrails=" + distanceInContrails + "]";
    }

    public ArrayList<Node> getPath() {
        return path;
    }



    public String getS() {
        return s;
    }





    
    

}
