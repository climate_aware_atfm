package weather;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Class for contrails data.
 */
public class Contrailsdata {
    private final String FILE_NAME;

    private HashMap<String, HashMap<String, Double>> accf;
    private HashMap<String, HashMap<String, Double>> pourcent;

    /**
     * Constructor extracting data from a file.
     * 
     * @param filename
     */
    public Contrailsdata(String filename) {
        this.FILE_NAME = filename;

        try {
            fromFile();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Get the accf value on an arc p1,p2
     * 
     * @param p1 id of p1
     * @param p2 id of p2
     * @return
     */
    public double getACCF(String p1, String p2) {
        return this.accf.get(p1).get(p2);
    }

    /**
     * Get the part on an arc p1,p2 in contrail areas
     * 
     * @param p1 id of p1
     * @param p2 id of p2
     * @return
     */
    public double getPourcent(String p1, String p2) {
        return this.pourcent.get(p1).get(p2);
    }

    /**
     * Extract data from a file.
     * 
     * @throws NumberFormatException
     * @throws IOException
     */
    private void fromFile() throws NumberFormatException, IOException {

        this.accf = new HashMap<>();
        this.pourcent = new HashMap<>();

        FileReader filereader = new FileReader(FILE_NAME);
        BufferedReader br = new BufferedReader(filereader);
        String line;

        String lastWord = null;

        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");

            if (!words[0].equals("p1")) {
                String p1 = words[0];
                String p2 = words[1];
                Double a = Double.valueOf(words[2]);
                Double p = Double.valueOf(words[3]);
                if (!p1.equals(lastWord)) {
                    lastWord = p1;
                    this.accf.put(p1, new HashMap<>());
                    this.pourcent.put(p1, new HashMap<>());
                }

                this.accf.get(p1).put(p2, Math.pow(10, 8) * a); // Avoiding numerical errors with small numbers
                this.pourcent.get(p1).put(p2, p);
            }

        }

        br.close();
    }

}
