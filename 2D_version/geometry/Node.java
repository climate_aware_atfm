package geometry;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import weather.WindData;

/**
 * Class represneting waypoints.
 */
public class Node {

    public static double TO_RADIANS = Math.PI / 180.;
    public static double TO_DEGREES = 180. / Math.PI;

    private int index;

    public int getIndex() {
        return index;
    }

    private double lon;

    public double getLon() {
        return lon;
    }

    private double lat;

    public double getLat() {
        return lat;
    }

    private int sector;

    public int getSector() {
        return sector;
    }

    private String id;

    public String getId() {
        return id;
    }

    private double lonRad;

    public double getLonRad() {
        return lonRad;
    }

    private double latRad;

    public double getLatRad() {
        return latRad;
    }

    private double wu;
    private double wv;

    public double getWu() {
        return wu;
    }

    public double getWv() {
        return wv;
    }

    /**
     * Constructor.
     * 
     * @param lon
     * @param lat
     * @param sector
     * @param id
     * @param wd
     * @param index
     */
    public Node(double lon, double lat, int sector, String id, WindData wd, int index) {
        this.lon = lon;
        this.lonRad = lon * TO_RADIANS;

        this.lat = lat;
        this.latRad = lat * TO_RADIANS;

        this.sector = sector;
        this.id = id;

        this.wu = wd.interpolateWu(this.lon, this.lat);
        this.wv = wd.interpolateWv(this.lon, this.lat);
        this.index = index;

    }

    /**
     * Great circle distance between two nodes.
     * 
     * @param other
     * @return
     */
    public double distance(Node other) {

        double d = 60. * TO_DEGREES * Math.acos(Math.sin(this.latRad) * Math.sin(other.getLatRad())
                + Math.cos(this.latRad) * Math.cos(other.getLatRad()) * Math.cos(this.lonRad - other.getLonRad()));
        return d;
    }

    /**
     * Euclidean distance between two nodes.
     * 
     * @param other
     * @return
     */
    public double distanceCartesian(Node other) {
        return Math.sqrt((this.lon - other.getLon()) * (this.lon - other.getLon())
                + (this.lat - other.getLat()) * (this.lat - other.getLat()));
    }

    /**
     * Flight time from a point to another giving an airspeed and wind data.
     * 
     * @param other
     * @param airspeed
     * @param wd
     * @return
     */
    public double time(Node other, double airspeed, WindData wd) {

        double d = this.distance(other);

        double wu0 = wd.interpolateWu(this.lon, this.lat);
        double wu1 = wd.interpolateWu(other.getLon(), other.getLat());
        double wv0 = wd.interpolateWv(this.lon, this.lat);
        double wv1 = wd.interpolateWv(other.getLon(), other.getLat());

        double wu = (wu0 + wu1) / 2.;
        double wv = (wv0 + wv1) / 2.;

        double dy = (this.lat - other.getLat());
        double dx = (this.lon - other.getLon());
        double dC = Math.sqrt(dx * dx + dy * dy);

        double vx = (-airspeed / dC) * dx + wu;
        double vy = (-airspeed / dC) * dy + wv;

        double v = Math.sqrt(vx * vx + vy * vy);

        if (Double.isNaN(v)) {
            return 0.;
        }

        return 60. * d / v;
    }

    /**
     * Time from a point to another when they are neighbors.
     * 
     * @param other
     * @param airspeed
     * @param arc
     * @return
     */
    public double time(Node other, double airspeed, Arc arc) {

        return arc.getTime(airspeed);
    }

    @Override
    public String toString() {
        return "Node [lon=" + lon + ", lat=" + lat + ", sector=" + sector + ", id=" + id + "]";
    }

    /**
     * Get the nodes from a file of waypoints data.
     * 
     * @param filename
     * @param sectors
     * @param wd
     * @return
     * @throws IOException
     */
    public static ArrayList<Node> getFromFile(String filename, ArrayList<Sector> sectors, WindData wd)
            throws IOException {
        ArrayList<Node> points = new ArrayList<>();
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        int i = 0;
        while ((line = br.readLine()) != null) {
            String[] words = line.split(",");
            if (!words[0].equals("name")) {
                String id = words[0];
                double lon = Double.valueOf(words[2]);
                double lat = Double.valueOf(words[1]);
                int sector = Integer.valueOf(words[3]);

                if (sector == -1) {
                    System.out.println("SECTOR ISSUE");
                } else {

                    points.add(new Node(lon, lat, sector, id, wd, i));
                    i++;
                }
            }
        }
        br.close();
        return points;

    }

}
