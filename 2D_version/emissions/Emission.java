package emissions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for emission data (fuelflow and co2)
 */
public class Emission {

    private HashMap<Integer, Float> fuelflow;
    private HashMap<Integer, Float> CO2;
    private ArrayList<Float> FFtab;

    private int minmass;

    /**
     * @param filename File in which data are.
     * @throws IOException
     */
    public Emission(String filename) throws IOException {
        this.fuelflow = new HashMap<>();
        this.CO2 = new HashMap<>();

        this.FFtab = new ArrayList<>();

        this.getFromFile(filename);

    }

    /**
     * @param filename File in which data are.
     * @throws IOException
     */
    private void getFromFile(String filename) throws IOException {
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        this.minmass = -1;
        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");
            if (!words[1].equals("mass")) {
                int mass = Integer.valueOf(words[1]);

                if (mass % 10 == 0) {
                    if (minmass == -1) {
                        minmass = mass / 10;
                    }
                    Float ff = Float.valueOf(words[2]);
                    Float co2 = Float.valueOf(words[3]);

                    this.fuelflow.put(mass / 10, ff);
                    this.FFtab.add(ff);

                    this.CO2.put(mass / 10, co2);

                }

            }
        }
        br.close();

    }

    /**
     * @param mass mass divided by 10
     * @return CO2 /s at the given mass
     */
    public double getCO2(int mass) {

        return this.CO2.get(mass);
    }

    /**
     * @param mass mass divided by 10
     * @return kg(fuel) /s at the given mass
     */
    public double getFuelFlow(int mass) {

        return this.FFtab.get(mass - minmass);

    }

}
