import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import aircraft.Flight;
import config.Config;
import geometry.Graph;
import geometry.Sector;
import ilog.concert.IloException;
import optimization.Para_MainProblem;
import optimization.MainProblem;

import weather.WindData;

import weather.ContrailsData;

public class MainClass {
    public static void main(String[] args) throws IOException, IloException, InterruptedException, ExecutionException {
        System.out.println("--------- Getting parameters ---------");
        Config p = new Config("data.txt");
        System.out.println(p);

        System.out.println("--------- DATA EXTRACTION ---------");

        String fileWeather = p.getFileWeather();

        String accf0 = p.getAccFFILe0();
        String accfm1 = p.getAccFFILe1();
        String accfm2 = p.getAccFFILe2();

        double LAT_MAX = p.getLAT_MAX();
        double LAT_MIN = p.getLAT_MIN();
        double LON_MAX = p.getLON_MAX();
        double LON_MIN = p.getLON_MIN();
        double DELTA_LON = p.getDELTA_LON();
        double DELTA_LAT = p.getDELTA_LAT();
        System.out.println("Weather data extraction...");

        WindData WIND_DATA = new WindData(fileWeather, LAT_MAX, LAT_MIN, LON_MAX, LON_MIN, DELTA_LAT, DELTA_LON);
        ContrailsData ACCF_DATA2 = new ContrailsData(accf0, accfm1, accfm2);
        System.out.println("Weather data extraction done.");

        System.out.println("Airspace data extraction...");
        String SECTORS_FILE = p.getSECTORS_FILE();
        String POINTS_FILE = p.getPOINTS_FILE();
        ArrayList<Sector> sectors = Sector.sectorsFromFile(SECTORS_FILE);
        Graph graph = new Graph(POINTS_FILE, sectors, WIND_DATA, ACCF_DATA2);
        System.out.println("Airspace data extraction done.");

        System.out.println("Input data extraction...");
        String AIRCRAFT_FILE = p.getAIRCRAFT_FILE();
        int nAv = p.getnAv();
        ArrayList<Flight> AIRCRAFTS;

        if (nAv >= 0) {
            AIRCRAFTS = Flight.getFromFile(AIRCRAFT_FILE, graph.getPoints(), nAv);
        } else {
            AIRCRAFTS = Flight.getFromFile(AIRCRAFT_FILE, graph.getPoints());
        }

        System.out.println("Input data extraction done.");

        System.out.println("OPTIMIZATION PROCESS...");
        String FILENAME_SAVE_VALUES = p.getFILENAME_SAVE_VALUES();
        String FILENAME_SAVE_PATHS = p.getFILENAME_SAVE_PATHS();

        if (p.getPARA() == 1) {
            Para_MainProblem pb = new Para_MainProblem(AIRCRAFTS, graph, WIND_DATA, ACCF_DATA2, p.getNT(),
                    FILENAME_SAVE_VALUES,
                    FILENAME_SAVE_PATHS, p);
        } else {
            MainProblem pb = new MainProblem(AIRCRAFTS, graph, WIND_DATA, ACCF_DATA2, p.getNT(),
                    FILENAME_SAVE_VALUES,
                    FILENAME_SAVE_PATHS, p);
        }

    }
}
