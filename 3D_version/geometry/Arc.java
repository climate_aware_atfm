package geometry;

import java.util.ArrayList;
import java.util.HashMap;

import weather.aCCFData;

/**
 * Arc for graph.
 */
public class Arc {

    private Node p1;
    private Node p2;

    private double distance;
    private double dc;
    private double dx;
    private double dy;

    private double wu;
    private double wv;

    private double theta;
    private double ctheta;
    private double stheta;

    private double accf;
    private double pourcent;

    private HashMap<Integer, ArrayList<Integer>> memo;

    public Node getP1() {
        return p1;
    }

    public Node getP2() {
        return p2;
    }

    public double getDistance() {
        return distance;
    }

    /**
     * @param airspeed
     * @return The flight time above the arc in function of the airspeed.
     */
    public double getTime(double airspeed) {

        double vx = (-(1 + 0.02 * this.p2.getLevel())*airspeed / dc) * dx + wu;
        double vy = (-(1 + 0.02 * this.p2.getLevel())*airspeed / dc) * dy + wv;
        double v = Math.sqrt(vx * vx + vy * vy);

        return 60. * distance / v;

    }

    /**
     * Construct the arc object.
     * 
     * @param p1 First point of the arc.
     * @param p2 Second point of the arc.
     * @param ad Data for contrails.
     */
    public Arc(Node p1, Node p2, aCCFData ad) {
        this.p1 = p1;
        this.p2 = p2;
        this.distance = p1.distance(p2);
        this.theta = Math.atan2(p2.getLat() - p1.getLat(), p2.getLon() - p1.getLon());
        this.ctheta = Math.cos(theta);
        this.stheta = Math.sin(theta);
        this.dc = p1.distanceCartesian(p2);
        this.dx = p1.getLon() - p2.getLon();
        this.dy = p1.getLat() - p2.getLat();

        this.memo = new HashMap<>();
        this.accf = ad.getACCF(p1.getId(), p2.getId(), p1.getLevel()) * this.distance * 1.852;
        this.pourcent = ad.getPourcent(p1.getId(), p2.getId(), p1.getLevel());

        this.wu = (p1.getWu() + p2.getWu()) / 2.;
        this.wv = (p1.getWv() + p2.getWv()) / 2.;
    }

    /**
     * Construct the arc object with the distance of the arc pre-computed.
     * 
     * @param p1 First point of the arc.
     * @param p2 Second point of the arc.
     * @param d  Pre-computed distance.
     * @param ad Data for contrails.
     */
    public Arc(Node p1, Node p2, double d, aCCFData ad) {
        this.p1 = p1;
        this.p2 = p2;
        this.distance = d;
        this.theta = Math.atan2(p2.getLat() - p1.getLat(), p2.getLon() - p1.getLon());
        this.ctheta = Math.cos(theta);
        this.stheta = Math.sin(theta);
        this.dc = p1.distanceCartesian(p2);
        this.memo = new HashMap<>();
        this.accf = ad.getACCF(p1.getId(), p2.getId(), p1.getLevel()) * this.distance * 1.852;
        this.pourcent = ad.getPourcent(p1.getId(), p2.getId(), p1.getLevel());
        this.dx = p1.getLon() - p2.getLon();
        this.dy = p1.getLat() - p2.getLat();
        this.wu = (p1.getWu() + p2.getWu()) / 2.;
        this.wv = (p1.getWv() + p2.getWv()) / 2.;
    }

    /**
     * @param sectors
     * @param nExtra
     * @return Get sectors crossed by the arc (with nExtra points for this)
     */
    public ArrayList<Integer> getSectorsCrossed(ArrayList<Sector> sectors, int nExtra) {
        if (this.memo.containsKey(nExtra)) {
            return this.memo.get(nExtra);
        }
        ArrayList<Integer> results = new ArrayList<>();

        double x0 = p1.getLon();
        double y0 = p1.getLat();

        double dt = dc / nExtra;

        int jS = 0;
        for (Sector s : sectors) {
            if (s.containsP(x0, y0)) {
                results.add(jS);
                break;
            }
            jS += 1;
        }

        double di = dt * this.ctheta;
        double dj = dt * this.stheta;

        for (int i = 1; i < nExtra; i++) {
            double x = p1.getLon() + i * di;
            double y = p1.getLat() + i * dj;

            int jS_ = 0;

            for (Sector s : sectors) {
                if (s.containsP(x, y)) {
                    results.add(jS_);

                    break;
                }
                jS_ += 1;
            }

        }
        this.memo.put(nExtra, results);
        return results;
    }

    /**
     * @param p1
     * @param p2
     * @param sectors
     * @param nExtra
     * @return Get sectors crossed by the arc (with nExtra points for this)
     */
    public static ArrayList<Integer> getSectorsCrossed(Node p1, Node p2, ArrayList<Sector> sectors, int nExtra) {
        ArrayList<Integer> results = new ArrayList<>();
        double x0 = p1.getLon();
        double y0 = p1.getLat();

        double dt = p1.distanceCartesian(p2) / nExtra;

        ArrayList<Double> listX = new ArrayList<>();
        listX.add(x0);
        ArrayList<Double> listY = new ArrayList<>();
        listY.add(y0);

        int jS = 0;
        int lastJS = 0;
        boolean found = false;
        for (Sector s : sectors) {
            if (s.containsP(x0, y0)) {
                results.add(jS);
                found = true;
                lastJS = jS;
                break;
            }
            jS += 1;
        }
        if (!found) {
            results.add(p1.getSector());
            lastJS = p1.getSector();
        }

        double theta = Math.atan2(p2.getLat() - p1.getLat(), p2.getLon() - p1.getLon());
        double di = dt * Math.cos(theta);
        double dj = dt * Math.sin(theta);

        for (int i = 1; i < nExtra; i++) {
            double x = p1.getLon() + i * di;
            double y = p1.getLat() + i * dj;

            listX.add(x);
            listY.add(y);
            int jS_ = 0;
            int test = -1;
            for (Sector s : sectors) {
                if (s.containsP(x, y)) {
                    results.add(jS_);
                    lastJS = jS_;
                    test = 0;
                    break;
                }
                jS_ += 1;
            }
            if (test == -1) {
                results.add(lastJS);
            }

        }

        return results;
    }

    /**
     * @return accf value above the arc.
     */
    public double getACCF() {

        return this.accf;
    }

    /**
     * @return Pourcentage of the arc in persistent contrail areas.
     */
    public double getPourcent() {
        return this.pourcent;
    }

}
