package geometry;

import icy.type.geom.Polygon2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class representing sector. They are only defined in 2D, the altitude does not
 * impact sectorization.
 */
public class Sector {

    private Polygon2D polygon;

    /**
     * Constructor from the polygon.
     * 
     * @param polygon
     */
    public Sector(Polygon2D polygon) {
        this.polygon = polygon;
    }

    /**
     * Get the list of sectors from a file.
     * 
     * @param filename
     * @return
     * @throws IOException
     */
    public static ArrayList<Sector> sectorsFromFile(String filename) throws IOException {
        ArrayList<Sector> sectors = new ArrayList<>();
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        while ((line = br.readLine()) != null) {
            String[] words = line.split(" - ");
            Polygon2D p = new Polygon2D();
            for (String s : words) { // Iteration on each coordinates
                String[] words2 = s.split(" , ");

                String intLatString = words2[0].substring(0, 2); // Always in the north, no problem here with the sign
                String minLatString = words2[0].substring(3, 5);
                String secLatString = words2[0].substring(6, 8);
                int intLat = Integer.valueOf(intLatString);
                int minLat = Integer.valueOf(minLatString);
                int secLat = Integer.valueOf(secLatString);
                double lat = intLat + minLat / 60. + secLat / 3600.;

                String signStr = words2[1].substring(10, 11);
                int sign = 1;
                if (signStr.equals("W")) {
                    sign = -1;
                }

                String intLonString = words2[1].substring(0, 3);
                String minLonString = words2[1].substring(4, 6);
                String secLonString = words2[1].substring(7, 9);
                int intLon = Integer.valueOf(intLonString);
                int minLon = Integer.valueOf(minLonString);
                int secLon = Integer.valueOf(secLonString);
                double lon = sign * (intLon + minLon / 60. + secLon / 3600.);

                p.addPoint(lon, lat);

            }
            sectors.add(new Sector(p));
        }

        return sectors;
    }

    /**
     * @return
     */
    public Polygon2D getPolygon() {
        return polygon;
    }

    /**
     * Check if a point is in the considered sector. Only 2D is considered.
     * 
     * @param lon
     * @param lat
     * @return
     */
    public boolean containsP(double lon, double lat) {
        return this.polygon.contains(lon, lat);
    }

}
