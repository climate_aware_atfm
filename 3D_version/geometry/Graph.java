package geometry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import weather.WindData;
import weather.aCCFData;

/**
 * Graph class.
 */
public class Graph {

    /**
     * Maximum distance between neighbors.
     */
    public static final double D_MAX = 130.;

    /**
     * Minimum distance between neighbors.
     */
    public static final double D_MIN = 40.;

    private ArrayList<Node> points;
    private HashMap<Node, ArrayList<Node>> adj;
    private HashMap<Node, ArrayList<Arc>> adjArc;

    private HashMap<Node, Integer> indexPoints;
    public HashMap<Node, HashMap<Node, Arc>> list_of_arcs;

    private ArrayList<Sector> sectors;

    /**
     * Constructor with a file of points.
     * 
     * @param filename
     * @param sectors
     * @param wd
     * @param ad
     * @throws IOException
     */
    public Graph(String filename, ArrayList<Sector> sectors, WindData wd, aCCFData ad) throws IOException {
        this.sectors = sectors;
        this.points = Node.getFromFile(filename, sectors, wd);
        this.indexPoints = new HashMap<>();
        this.list_of_arcs = new HashMap<>();
        this.adj = new HashMap<>();
        this.adjArc = new HashMap<>();
        for (int i = 0; i < this.points.size(); i++) {
            this.indexPoints.put(this.points.get(i), i);
        }
        int nArc = 0;
        for (Node p1 : this.points) {
            HashMap<Node, Arc> h = new HashMap<>();
            this.adj.put(p1, new ArrayList<>());
            this.adjArc.put(p1, new ArrayList<>());
            this.list_of_arcs.put(p1, new HashMap<>());
            for (Node p2 : this.points) {
                double d = p1.distance(p2);
                if ((d >= D_MIN) && (d <= D_MAX) && (p1.getLevel() == p2.getLevel())) {
                    adj.get(p1).add(p2);
                    Arc a = new Arc(p1, p2, d, ad);
                    adjArc.get(p1).add(a);
                    nArc += 1;
                    h.put(p2, a);
                }
            }
            list_of_arcs.put(p1, h);
        }

    }

    /**
     * @param p1
     * @param p2
     * @return
     */
    public Arc fromNodes(Node p1, Node p2) {
        return this.list_of_arcs.get(p1).get(p2);
    }

    /**
     * @return
     */
    public ArrayList<Node> getPoints() {
        return this.points;
    }

    /**
     * @param p
     * @return
     */
    public ArrayList<Node> getNeighbors(Node p) {
        return this.adj.get(p);
    }

    /**
     * @param p
     * @return
     */
    public ArrayList<Arc> getNeighborsArcs(Node p) {
        return this.adjArc.get(p);
    }

    /**
     * @return
     */
    public ArrayList<Sector> getSectors() {
        return sectors;
    }

    /**
     * @param n
     * @return
     */
    public int getIndex(Node n) {
        return this.indexPoints.get(n);
    }

}
