package emissions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for emission data (fuelflow and co2)
 */
public class Emission {

    // Data for the highest FL
    private HashMap<Integer, Float> fuelflow0;
    private HashMap<Integer, Float> CO20;

    // Data for the first FL under the highest one
    private HashMap<Integer, Float> fuelflowm1;
    private HashMap<Integer, Float> CO2m1;

    // Data for the second FL under the highest one
    private HashMap<Integer, Float> fuelflowm2;
    private HashMap<Integer, Float> CO2m2;

    private int minmass;

    private ArrayList<Float> fuelflowm2_;
    private ArrayList<Float> fuelflowm1_;
    private ArrayList<Float> fuelflow0_;

    /**
     * @param filename File in which data are.
     * @throws IOException
     */
    public Emission(String filename) throws IOException {
        this.fuelflow0 = new HashMap<>();
        this.CO20 = new HashMap<>();

        this.fuelflowm1 = new HashMap<>();
        this.CO2m1 = new HashMap<>();

        this.fuelflowm2 = new HashMap<>();
        this.CO2m2 = new HashMap<>();

        this.fuelflow0_ = new ArrayList<>();
        this.fuelflowm2_ = new ArrayList<>();
        this.fuelflowm1_ = new ArrayList<>();

        this.getFromFile(filename);

    }

    /**
     * @param filename File in which data are.
     * @throws IOException
     */
    private void getFromFile(String filename) throws IOException {
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        int i = 0;
        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");
            if (!words[1].equals("mass")) {
                int mass = Integer.valueOf(words[1]);
                if (mass % 10 == 0) {
                    if (i == 0) {
                        minmass = mass / 10;
                        i += 1;
                    }

                    Float ff0 = Float.valueOf(words[2]);

                    Float co20 = Float.valueOf(words[3]);

                    Float ffm1 = Float.valueOf(words[4]);
                    Float ffp1 = Float.valueOf(words[6]);
                    Float co2m1 = Float.valueOf(words[5]);
                    Float CO2m2 = Float.valueOf(words[7]);

                    this.fuelflow0.put(mass / 10, ff0);
                    this.CO20.put(mass / 10, co20);

                    this.fuelflowm1.put(mass / 10, ffm1);
                    this.CO2m1.put(mass / 10, co2m1);

                    this.fuelflowm2.put(mass / 10, ffp1);
                    this.CO2m2.put(mass / 10, CO2m2);
                    this.fuelflow0_.add(ff0);
                    this.fuelflowm1_.add(ffm1);
                    this.fuelflowm2_.add(ffp1);

                }

            }
        }
        br.close();

    }

    /**
     * @param mass  mass divided by 10
     * @param level
     * @return CO2 /s at the given mass
     */
    public double getCO2(int mass, int level) {

        if (level == 0) {
            return this.CO20.get(mass);
        } else if (level == -1) {
            return this.CO2m1.get(mass);
        } else {
            return this.CO2m2.get(mass);
        }

    }

    /**
     * @param mass mass divided by 10
     * @return kg(fuel) /s at the given mass
     */
    public double getFuelFlow(int mass, int level) {

        if (level == 0) {

            return this.fuelflow0_.get(mass - minmass);
        } else if (level == -1) {

            return this.fuelflowm1_.get(mass - minmass);
        } else {

            return this.fuelflowm2_.get(mass - minmass);
        }

    }

}
