package config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class for configuration file process.
 */
public class Config {
    private double BIG_M;

    private double EPS;
    private double CAPACITY;
    private int MAX_ITER;
    private double DT;
    private double INF;
    private double INF_2;
    private int NT;

    private String fileWeather;
    private String accFFILe0;
    private String accFFILe1;
    private String accFFILe2;

    private double LAT_MAX;
    private double LAT_MIN;
    private double LON_MAX;
    private double LON_MIN;
    private double DELTA_LON;
    private double DELTA_LAT;

    private String FILENAME_SAVE_VALUES;
    private String FILENAME_SAVE_PATHS;

    private String SECTORS_FILE;
    private String POINTS_FILE;
    private int nAv;
    private String AIRCRAFT_FILE;

    private int PARA;

    private String cost;

    /**
     * @param configFile Configuration file.
     * @throws IOException
     */
    public Config(String configFile) throws IOException {
        fromFile(configFile);
    }

    /**
     * Extract data from the file.
     * 
     * @param configFile Configuration file.
     * @throws IOException
     */
    private void fromFile(String configFile) throws IOException {
        FileInputStream fstream = new FileInputStream(configFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        String[] strs;

        // BIG_M : Big M value
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.BIG_M = (Double.valueOf(strs[1]));

        // EPS : Epsilon for near to 0 values
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.EPS = (Double.valueOf(strs[1]));

        // CAPACITY : airspace capacity
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.CAPACITY = (Double.valueOf(strs[1]));

        // MAX_ITER (int) : maximum number of iterations
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.MAX_ITER = (Integer.valueOf(strs[1]));

        // DT : time step discretization
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.DT = (Double.valueOf(strs[1]));

        // INF : infinite value
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.INF = (Double.valueOf(strs[1]));

        // INF_2 : big value to represent infinite
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.INF_2 = (Double.valueOf(strs[1]));

        // NT (int) : maximum number of steps
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.NT = (Integer.valueOf(strs[1]));

        // fileWeather : weather file for wind
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.fileWeather = strs[1];

        // accFFILe : file for accf values and values for contrails
        // For the FL level the highest
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.accFFILe0 = strs[1];

        // For the first FL level under the highest
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.accFFILe1  = strs[1];

        // For the second FL level under the highest
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.accFFILe2 = strs[1];


        // LAT_MAX
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.LAT_MAX = (Double.valueOf(strs[1]));

        // LAT_MIN
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.LAT_MIN = (Double.valueOf(strs[1]));

        // LON_MAX
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.LON_MAX = (Double.valueOf(strs[1]));

        // LON_MIN
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.LON_MIN = (Double.valueOf(strs[1]));

        // DELTA_LON
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.DELTA_LON = (Double.valueOf(strs[1]));

        // DELTA_LAT
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.DELTA_LAT = (Double.valueOf(strs[1]));

        // FILENAME_SAVE_VALUES : file in which values are saved
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.FILENAME_SAVE_VALUES = strs[1];

        // FILENAME_SAVE_PATHS : file in which paths are saved
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.FILENAME_SAVE_PATHS = strs[1];

        // SECTORS_FILE : file in which sector polygons are defined
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.SECTORS_FILE = strs[1];

        // POINTS_FILE : file in which points are defined
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.POINTS_FILE = strs[1];

        // NAV : number of aircraft considered (if equal to -1, then all aircraft of the
        // file)
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.nAv = (Integer.valueOf(strs[1]));

        // AIRCRAFT_FILE : file in which flight data are defined
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.AIRCRAFT_FILE = strs[1];

        // PARA : equal to 1 if parallelized version else 0
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.PARA = Integer.valueOf(strs[1]);

        // COST FOR CONTRAILS : gwp20, gwp100, accf
        strLine = br.readLine();
        strs = strLine.split(" : ");
        this.cost = strs[1];

        fstream.close();
    }

    /**
     * @return
     */
    public String getSECTORS_FILE() {
        return SECTORS_FILE;
    }

    /**
     * @return
     */
    public int getnAv() {
        return nAv;
    }

    /**
     * @return
     */
    public String getPOINTS_FILE() {
        return POINTS_FILE;
    }

    /**
     * @return
     */
    public String getFileWeather() {
        return fileWeather;
    }

    /**
     * @return
     */
    public String getFILENAME_SAVE_VALUES() {
        return FILENAME_SAVE_VALUES;
    }

    /**
     * @return
     */
    public String getFILENAME_SAVE_PATHS() {
        return FILENAME_SAVE_PATHS;
    }

    /**
     * @return
     */
    public double getLAT_MAX() {
        return LAT_MAX;
    }

    /**
     * @return
     */
    public double getLAT_MIN() {
        return LAT_MIN;
    }

    /**
     * @return
     */
    public double getLON_MAX() {
        return LON_MAX;
    }

    /**
     * @return
     */
    public double getLON_MIN() {
        return LON_MIN;
    }

    /**
     * @return
     */
    public double getDELTA_LON() {
        return DELTA_LON;
    }

    /**
     * @return
     */
    public double getDELTA_LAT() {
        return DELTA_LAT;
    }

    /**
     * @return
     */
    public String getAccFFILe0() {
        return accFFILe0;
    }

    /**
     * @return
     */
    public String getAccFFILe1() {
        return accFFILe1;
    }

    /**
     * @return
     */
    public String getAccFFILe2() {
        return accFFILe2;
    }

    /**
     * @return
     */
    public double getBIG_M() {
        return BIG_M;
    }

    /**
     * @return
     */
    public double getEPS() {
        return EPS;
    }

    /**
     * @return
     */
    public double getCAPACITY() {
        return CAPACITY;
    }

    /**
     * @return
     */
    public int getMAX_ITER() {
        return MAX_ITER;
    }

    /**
     * @return
     */
    public double getDT() {
        return DT;
    }

    /**
     * @return
     */
    public double getINF() {
        return INF;
    }

    /**
     * @return
     */
    public double getINF_2() {
        return INF_2;
    }

    /**
     * @return
     */
    public int getNT() {
        return NT;
    }

    /**
     * @return
     */
    public String getAIRCRAFT_FILE() {
        return AIRCRAFT_FILE;
    }

    /**
     * @return
     */
    public int getPARA() {
        return PARA;
    }

    /**
     * @return
     */
    public String getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Config [BIG_M=" + BIG_M + ", EPS=" + EPS + ", CAPACITY=" + CAPACITY + ", MAX_ITER=" + MAX_ITER + ", DT="
                + DT + ", INF=" + INF + ", INF_2=" + INF_2 + ", NT=" + NT + ", fileWeather=" + fileWeather
                + ", accFFILe0=" + accFFILe0 + ", accFFILe1=" + accFFILe1 + ", accFFILe2=" + accFFILe2 + ", LAT_MAX="
                + LAT_MAX + ", LAT_MIN=" + LAT_MIN + ", LON_MAX=" + LON_MAX + ", LON_MIN=" + LON_MIN + ", DELTA_LON="
                + DELTA_LON + ", DELTA_LAT=" + DELTA_LAT + ", FILENAME_SAVE_VALUES=" + FILENAME_SAVE_VALUES
                + ", FILENAME_SAVE_PATHS=" + FILENAME_SAVE_PATHS + ", SECTORS_FILE=" + SECTORS_FILE + ", POINTS_FILE="
                + POINTS_FILE + ", nAv=" + nAv + ", AIRCRAFT_FILE=" + AIRCRAFT_FILE + ", PARA=" + PARA + ", cost="
                + cost + "]";
    }

   

}