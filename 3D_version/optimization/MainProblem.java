package optimization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import aircraft.Flight;
import config.Config;
import emissions.Emission;
import geometry.Arc;
import geometry.Graph;
import geometry.Node;
import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;

import weather.WindData;
import weather.aCCFData;

public class MainProblem {

    private ArrayList<Flight> aircrafts;
    private HashMap<Integer, ArrayList<IloNumVar>> variables;
    private HashMap<IloNumVar, ArrayList<Node>> correspondingPath;
    private HashMap<Integer, ArrayList<Double>> costs;
    private HashMap<Integer, ArrayList<Double>> emissionsCO2;
    private HashMap<Integer, ArrayList<Double>> times;
    private HashMap<Integer, ArrayList<Double>> distances;
    private HashMap<Integer, ArrayList<Double>> timesInContrails;
    private HashMap<Integer, ArrayList<Double>> distancesInContrails;
    private HashMap<Integer, HashMap<Integer, ArrayList<IloNumVar>>> pst;

    private IloCplex cplex;

    private int N;
    private int nS;
    private int nT;

    private final double BIG_M;
    private final double CAPACITY;
    private final double DT;
    private final int MAX_ITER;
    private final double EPS;

    /**
     * Constructor, the problem is also solved in this method
     * 
     * @param aircrafts
     * @param graph
     * @param wd
     * @param ad
     * @param nT
     * @param filenameValues
     * @param filenamePaths
     * @param p
     * @throws IloException
     * @throws IOException
     */
    public MainProblem(ArrayList<Flight> aircrafts, Graph graph, WindData wd, aCCFData ad, int nT,
            String filenameValues, String filenamePaths, Config p) throws IloException, IOException {
        this.cplex = new IloCplex();
        this.aircrafts = aircrafts;
        this.N = aircrafts.size();
        this.nS = graph.getSectors().size();
        this.nT = nT;

        this.BIG_M = p.getBIG_M();
        this.CAPACITY = p.getCAPACITY();
        this.DT = p.getDT();
        this.MAX_ITER = p.getMAX_ITER();
        this.EPS = p.getEPS();

        // Initializing variables and costs
        this.variables = new HashMap<>();
        this.correspondingPath = new HashMap<>();
        this.costs = new HashMap<>();
        this.emissionsCO2 = new HashMap<>();
        this.times = new HashMap<>();
        this.distances = new HashMap<>();
        this.timesInContrails = new HashMap<>();
        this.distancesInContrails = new HashMap<>();
        for (int i = 0; i < N; i++) {
            variables.put(i, new ArrayList<>());
            costs.put(i, new ArrayList<>());
            emissionsCO2.put(i, new ArrayList<>());
            times.put(i, new ArrayList<>());
            distances.put(i, new ArrayList<>());
            timesInContrails.put(i, new ArrayList<>());
            distancesInContrails.put(i, new ArrayList<>());
            String name = "yy0_" + i;
            variables.get(i).add(cplex.numVar(0, 1, name));
            costs.get(i).add(BIG_M);
            emissionsCO2.get(i).add(BIG_M);
            times.get(i).add(BIG_M);
            distances.get(i).add(BIG_M);
            timesInContrails.get(i).add(BIG_M);
            distancesInContrails.get(i).add(BIG_M);

        }

        this.pst = new HashMap<>();
        for (int i = 0; i < nT; i++) {
            pst.put(i, new HashMap<>());
            for (int j = 0; j < nS; j++) {
                pst.get(i).put(j, new ArrayList<>());
            }
        }

        // Constraints definition
        IloRange[] constraints1 = new IloRange[N];
        for (int i = 0; i < N; i++) {
            constraints1[i] = cplex.addEq(cplex.sum(variables.get(i).toArray(new IloNumVar[variables.get(i).size()])),
                    1.);
        }

        IloRange[][] constraints2 = new IloRange[nT][nS];
        for (int i = 0; i < nT; i++) {
            for (int j = 0; j < nS; j++) {
                IloNumExpr s = cplex.sum(pst.get(i).get(j).toArray(new IloNumVar[pst.get(i).get(j).size()]));

                constraints2[i][j] = cplex.addLe(s, CAPACITY);

            }
        }

        ArrayList<IloNumVar> vars = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            for (IloNumVar v : variables.get(i)) {
                vars.add(v);
            }
        }
        double[] costsObj = new double[vars.size()];
        int k = 0;
        for (int i = 0; i < N; i++) {
            for (Double c : costs.get(i)) {
                costsObj[k] = c;
                k += 1;
            }
        }

        cplex.addMinimize();
        IloNumExpr obj = cplex.scalProd(vars.toArray(new IloNumVar[vars.size()]), costsObj);
        cplex.getObjective().setExpr(obj);

        // Adding shortest paths in the first restricted problem
        double[][] lambdas = new double[nT][nS];
        for (int i = 0; i < nT; i++) {
            for (int j = 0; j < nS; j++) {
                lambdas[i][j] = 0;
            }
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
            SubProblemResult ri = SubProblem.solvingSubProblem(aircrafts.get(i), lambdas, nT, nS, graph, wd,
                    ad, p);
            String name = "y" + variables.get(i).size() + "___" + i;
            IloNumVar newVar = cplex.numVar(0, 1, name);
            variables.get(i).add(newVar);
            correspondingPath.put(newVar, ri.getPath());
            costs.get(i).add(ri.getCost());
            emissionsCO2.get(i).add(ri.getEmissionsCO2());
            times.get(i).add(ri.getTime());
            distances.get(i).add(ri.getDistance());
            timesInContrails.get(i).add(ri.getTimeInContrails());
            distancesInContrails.get(i).add(ri.getDistanceInContrails());

            int l = (int) (aircrafts.get(i).getDepartureTime() / DT);

            for (Integer s : ri.getSectorsCrossed()) {
                if (s != -1) {
                    pst.get(l).get(s).add(newVar);
                    IloNumExpr newExpr = cplex
                            .sum(pst.get(l).get(s).toArray(new IloNumVar[pst.get(l).get(s).size()]));
                    constraints2[l][s].setExpr(newExpr);
                }

                l += 1;
            }

            constraints1[i].setExpr(cplex.sum(constraints1[i].getExpr(), newVar));

            IloNumExpr newObj = cplex.sum(cplex.getObjective().getExpr(), cplex.prod(newVar, ri.getCost()));
            cplex.getObjective().setExpr(newObj);
        }
        long elapsed = System.currentTimeMillis() - start;
        System.out.println(String.format("Elapsed time: %d ms", elapsed));

        // First solving of main problem
        System.out.println("Main pb first resolution...");
        long t0 = System.currentTimeMillis();
        cplex.setParam(IloCplex.Param.RootAlgorithm, IloCplex.Algorithm.Primal);
        cplex.solve();
        double[] mus = cplex.getDuals(constraints1);

        for (int i = 0; i < nT; i++) {
            for (int j = 0; j < nS; j++) {
                lambdas[i][j] = (cplex.getDual(constraints2[i][j]));
            }
        }

        boolean finished = false;
        int nbAircraftDone = 0;
        int nbIter = 1;

        int iLast = 0;
        // iteration LOOP
        while (!finished) {

            System.out.println(" -----------------------------------" + " Iteration " + nbIter
                    + " -----------------------------------");

            for (int iii = 0; iii < N; iii++) {
                int i = (iLast + iii) % N;
                SubProblemResult ri = SubProblem.solvingSubProblem(aircrafts.get(i), lambdas, nT, nS, graph, wd,
                        ad, p);
                // In that case, the variable is not improving
                if (ri.getCostSP() - mus[i] > -EPS) {

                    nbAircraftDone += 1;
                } else {

                    String name = "y" + variables.get(i).size() + "_" + i;
                    IloNumVar newVar = cplex.numVar(0, 1, name);
                    variables.get(i).add(newVar);
                    correspondingPath.put(newVar, ri.getPath());
                    costs.get(i).add(ri.getCost());
                    emissionsCO2.get(i).add(ri.getEmissionsCO2());
                    times.get(i).add(ri.getTime());
                    distances.get(i).add(ri.getDistance());
                    timesInContrails.get(i).add(ri.getTimeInContrails());
                    distancesInContrails.get(i).add(ri.getDistanceInContrails());

                    int l = (int) (aircrafts.get(i).getDepartureTime() / DT);
                    // Checking the crossed sectors and adding the members in the constraints
                    for (Integer s : ri.getSectorsCrossed()) {
                        if (s != -1) {
                            pst.get(l).get(s).add(newVar);
                            IloNumExpr newExpr = cplex
                                    .sum(pst.get(l).get(s).toArray(new IloNumVar[pst.get(l).get(s).size()]));
                            constraints2[l][s].setExpr(newExpr);
                        }

                        l += 1;
                    }

                    constraints1[i].setExpr(cplex.sum(constraints1[i].getExpr(), newVar));

                    IloNumExpr newObj = cplex.sum(cplex.getObjective().getExpr(), cplex.prod(newVar, ri.getCost()));
                    cplex.getObjective().setExpr(newObj);

                    cplex.solve();
                    printSolution(false, false);
                    mus = cplex.getDuals(constraints1);

                    for (int ii = 0; ii < nT; ii++) {
                        for (int jj = 0; jj < nS; jj++) {
                            lambdas[ii][jj] = (cplex.getDual(constraints2[ii][jj]));

                        }
                    }

                    iLast = (i + 1) % N;
                    break;
                }

            }

            finished = (nbAircraftDone == N);
            nbAircraftDone = 0;
            nbIter += 1;

            if (nbIter >= MAX_ITER) {
                break;
            }

        }
        long tf = System.currentTimeMillis();
        System.out.println("\nOptimization process finished.");

        long dtComputation = tf - t0;
        System.out.println("Computation time : " + dtComputation + " ms.");
        if (finished) {
            System.out.println("CONTINUOUS SOLUTION FOUND.");
            boolean testInteger = isIntegerSolution();
            if (testInteger) {
                System.out.println("INTEGER SOLUTION FOUND.");
                printSolution(false, true);
                System.out.println("FILE SAVING...");
                saveToFile(filenameValues, filenamePaths, graph);
                System.out.println("FILE SAVING DONE.");
            } else {
                if (solveIntModel()) {
                    saveToFile(filenameValues, filenamePaths, graph);
                    System.out.println("INTEGER SOLUTION FOUND BY INTEGER MODEL.");
                    System.out.println(this.cplex.getObjValue());
                }

            }
        }

    }

    /**
     * Evaluate a path flown by an aircraft on the graph. (to compare solutions)
     * 
     * @param path
     * @param ac
     * @param graph
     * @return
     */
    private double[] evaluatePath(ArrayList<Node> path, Flight ac, Graph graph) {
        double distance = 0.;
        double time = 0.;
        double timeInContrails = 0.;
        double distanceInContrails = 0.;
        double co2 = 0.;
        double accf_contrails = 0.;
        double accf_co2 = 0.;
        double ACCF_CO2 = 6.94 * Math.pow(10, -5);
        double v = ac.getAirspeed();
        Emission em = ac.getEmissions();
        double mass = ac.getMass();
        for (int i = 1; i < path.size(); i++) {
            Node p1 = path.get(i - 1);
            Node p2 = path.get(i);
            Arc a = graph.fromNodes(p1, p2);
            distance += a.getDistance();
            double t = a.getTime(v);
            time += t;
            timeInContrails += a.getPourcent() * t;
            distanceInContrails += a.getDistance() * a.getPourcent();
            double f = em.getFuelFlow((int) (mass / 10.), a.getP1().getLevel());
            double co2_ = em.getCO2((int) (mass / 10.), a.getP1().getLevel());
            mass = mass - f * t * 60.;
            co2 += co2_;
            accf_contrails += a.getACCF();
            accf_co2 += f * 60. * t * ACCF_CO2;
        }

        accf_contrails = accf_contrails * Math.pow(10, -8);
        accf_co2 = accf_co2 * Math.pow(10, -8);

        return new double[] { distance, time, timeInContrails, distanceInContrails, co2, accf_contrails, accf_co2 };

    }

    /**
     * Check if the solution obtained by the process is an integer one or not.
     * 
     * @return
     * @throws UnknownObjectException
     * @throws IloException
     */
    private boolean isIntegerSolution() throws UnknownObjectException, IloException {
        boolean testInteger = true;
        for (int i = 0; i < N; i++) {
            for (IloNumVar var : variables.get(i)) {
                if (Math.abs(((int) this.cplex.getValue(var)) - this.cplex.getValue(var)) > EPS) {
                    testInteger = false;
                }
            }
        }
        return testInteger;
    }

    /**
     * Print the solution obtained at the end of the process.
     * 
     * @param printDetails
     * @param printSector
     * @throws UnknownObjectException
     * @throws IloException
     */
    private void printSolution(boolean printDetails, boolean printSector) throws UnknownObjectException, IloException {
        double sObj = 0.;
        double sCO2 = 0.;
        double sDistance = 0.;
        double sTime = 0.;

        double tC = 0.;
        double dC = 0.;

        System.out.println(this.cplex.getObjValue());

        for (int i = 0; i < N; i++) {
            boolean f = false;
            for (int j = 0; j < variables.get(i).size(); j++) {
                IloNumVar var = variables.get(i).get(j);
                if (i == 0) {
                    f = true;
                    System.out.println(cplex.getValue(var));
                }
                if (cplex.getValue(var) > EPS) {
                    if (cplex.getValue(var) < 1) {
                        System.out.print(i + " " + cplex.getValue(var) + "-");
                    }

                    if (printDetails) {
                        System.out.println("\nAircraft " + i + " = ");
                        System.out.println("Cost function value : " + costs.get(i).get(j));
                        // System.out.println("CO2 emissions (g) : " + emissionsCO2.get(i).get(j));
                        // System.out.println("Distance flown (NM) : " + distances.get(i).get(j));
                        // System.out.println("Flight time (minutes) : " + times.get(i).get(j));
                        String path = "";
                        for (Node n : correspondingPath.get(var)) {
                            path += n.getId() + " ( " + n.getLon() + " , " + n.getLat() + "," + n.getLevel()
                                    + " )  --- ";
                        }
                        System.out.println(path);
                    }

                    sObj += costs.get(i).get(j);

                    tC += timesInContrails.get(i).get(j);
                    dC += distancesInContrails.get(i).get(j);

                    sCO2 += emissionsCO2.get(i).get(j);

                    sDistance += distances.get(i).get(j);

                    sTime += times.get(i).get(j);

                    // break;
                }
            }
            if (f) {
                System.out.println(" ");
            }

        }

        if (printSector) {
            for (int l = 0; l < this.nS; l++) {
                System.out.println("Sector : " + l);
                for (int t = 0; t < this.nT; t++) {
                    System.out.print(""
                            + this.cplex.getValue(this.cplex
                                    .sum(pst.get(t).get(l).toArray(new IloNumVar[this.pst.get(t).get(l).size()])))
                            + " ");
                }
                System.out.print('\n');
            }
        }

        System.out.println("\nTotal objective function : " + sObj);
        // System.out.println("Total CO2 (g) : " + sCO2);
        // System.out.println("Total Distance (NM) : " + sDistance);
        // System.out.println("Total Time (minutes) : " + sTime);
        // System.out.println("Total Distance in Contrails (NM) : " + dC);
        // System.out.println("Total Time in Contrails (minutes) : " + tC);
    }

    /**
     * Save results in two files (one for values and one for paths)
     * 
     * @param filenameValues
     * @param filenamePath
     * @param graph
     * @throws IOException
     * @throws UnknownObjectException
     * @throws IloException
     */
    private void saveToFile(String filenameValues, String filenamePath, Graph graph)
            throws IOException, UnknownObjectException, IloException {

        // Value saving in a csv file
        File csvFileV = new File(filenameValues);
        FileWriter fileWriterV = new FileWriter(csvFileV);

        File fileP = new File(filenamePath);
        FileWriter fileWriterP = new FileWriter(fileP);

        String header = "id,distance,time,tc,dc,co2,accf_c,accf_co2\n";
        fileWriterV.write(header);
        for (int i = 0; i < N; i++) {
            String line = "" + i + ",";
            for (int j = 0; j < variables.get(i).size(); j++) {
                IloNumVar var = variables.get(i).get(j);
                if (cplex.getValue(var) > EPS) {

                    double[] eval = evaluatePath(this.correspondingPath.get(var), aircrafts.get(i), graph);

                    line += "" + eval[0] + ",";
                    line += "" + eval[1] + ",";
                    line += "" + eval[2] + ",";
                    line += "" + eval[3] + ",";
                    line += "" + eval[4] + ",";
                    line += "" + eval[5] + ",";
                    line += "" + eval[6] + "\n";

                    fileWriterV.write(line);

                    String path = "" + i + " : ";
                    for (Node n : this.correspondingPath.get(var)) {
                        path += "(" + n.getId() + "," + n.getLon() + "," + n.getLat() + "," + n.getLevel() + ") ";
                    }
                    path += "\n";
                    fileWriterP.write(path);

                    break;
                }
            }
        }

        fileWriterV.close();
        fileWriterP.close();
    }

    /**
     * Transforming the problem into an integer problem and trying to solve it.
     * 
     * @return Solved or not
     * @throws IloException
     */
    private boolean solveIntModel() throws IloException {
        for (int i = 0; i < N; i++) {
            boolean f = false;
            for (int j = 0; j < variables.get(i).size(); j++) {
                IloNumVar var = variables.get(i).get(j);
                this.cplex.add(cplex.conversion(var, IloNumVarType.Int));
            }
        }
        if (this.cplex.solve()) {
            printSolution(true, false);
            return true;
        }
        return false;

    }

}
