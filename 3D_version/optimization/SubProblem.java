package optimization;

import java.util.ArrayList;
import java.util.Arrays;
import aircraft.Flight;
import config.Config;
import geometry.Arc;
import geometry.Graph;
import geometry.Node;
import geometry.Sector;

import weather.WindData;

import weather.aCCFData;

/**
 * Subproblem class.
 */
public class SubProblem {

    public static final double ACCF_CO2 = 6.94 * Math.pow(10, -5);

    /**
     * Solving the subproblem considered with the different parameters.
     * 
     * @param aircraft
     * @param lambdas
     * @param nT
     * @param nS
     * @param graph
     * @param wd
     * @param ad
     * @param p
     * @return
     */
    public static SubProblemResult solvingSubProblem(Flight aircraft, double[][] lambdas, int nT, int nS, Graph graph,
            WindData wd, aCCFData ad, Config p) {

        double INF = p.getINF();
        double INF2 = p.getINF_2();
        double DT = p.getDT();
        int nPoints = graph.getPoints().size();
        double[][] costsSP = new double[nPoints][nT];
        double[][] costs = new double[nPoints][nT];

        double[][] masses = new double[nPoints][nT];

        int[][] predecessorsP = new int[nPoints][nT];
        int[][] predecessorsT = new int[nPoints][nT];

        int indexTDep = (int) (aircraft.getDepartureTime() / DT);

        int maxT = indexTDep + 1 + (int) (2. * 60
                * (aircraft.getPointDep().distance(aircraft.getPointArr()) / (DT * aircraft.getAirspeed())));
        maxT = Math.min(nT, maxT);

        // Costs initialization
        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < nT; j++) {
                if (graph.getPoints().get(i).getId().equals(aircraft.getPointDepId())) {
                    if (j == indexTDep) {

                        costsSP[i][j] = 0;
                        costs[i][j] = 0;

                        masses[i][j] = aircraft.getMass();

                    } else {
                        costsSP[i][j] = INF;
                        costs[i][j] = INF;

                        masses[i][j] = INF;

                    }
                } else {
                    costsSP[i][j] = INF;
                    costs[i][j] = INF;

                    masses[i][j] = INF;

                }
            }
        }

        int t = indexTDep;

        // Dynamic programming process
        while (t < maxT) {

            for (int i = 0; i < nPoints; i++) {

                Node p1 = graph.getPoints().get(i);
                if (aircraft.isProhibited(p1)) {
                    continue;
                }
                if (costs[i][t] > INF2) {
                    continue;
                }

                for (Arc a : graph.getNeighborsArcs(p1)) {
                    Node p2 = a.getP2();
                    if (aircraft.isProhibited(p2)) {
                        continue;
                    }
                    int j = graph.getIndex(a.getP2());

                    double[] t_dt = computeDt(aircraft, a, wd, DT);

                    int dt = (int) t_dt[1];
                    double tArc = t_dt[0];

                    double m = masses[i][t];

                    if ((t + dt < maxT) && (m < INF)) {
                        int mint = (int) (m / 10.);
                        double ff = aircraft.getEmissions().getFuelFlow(mint, p2.getLevel());

                        double[] cs = costArcSP(aircraft, a, wd, ad, lambdas, t, dt, graph.getSectors(), mint, tArc,
                                p.getCost(), ff);
                        double c = cs[1];

                        if (costsSP[j][t + dt] > costsSP[i][t] + c) {
                            costsSP[j][t + dt] = costsSP[i][t] + c;
                            predecessorsP[j][t + dt] = i;
                            predecessorsT[j][t + dt] = t;

                            masses[j][t + dt] = m
                                    - ff * tArc
                                            * 60.;

                            costs[j][t + dt] = costs[i][t] + cs[0];

                        }
                    }

                }

            }

            t += 1;

        }

        // Path and solution recovery
        ArrayList<Node> pathInversed = new ArrayList<>();

        int indexP = graph.getIndex(aircraft.getPointArr());
        int tP_1 = argmin(costsSP, nPoints, nT, indexP, INF); // For the different level, and then checking which is the
                                                              // best level
        int tP_0 = argmin(costsSP, nPoints, nT, indexP + 1, INF);
        int tP_2 = argmin(costsSP, nPoints, nT, indexP + 2, INF);

        int tP = -1;

        if ((costsSP[indexP + 1][tP_0] <= costsSP[indexP][tP_1])
                && (costsSP[indexP + 1][tP_0] <= costsSP[indexP + 2][tP_2])) {
            tP = tP_0;
            indexP = indexP + 1;

        } else if ((costsSP[indexP][tP_1] <= costsSP[indexP + 1][tP_0])
                && (costsSP[indexP][tP_1] <= costsSP[indexP + 2][tP_2])) {
            tP = tP_1;

        } else {
            tP = tP_2;
            indexP = indexP + 2;

        }

        pathInversed.add(graph.getPoints().get(indexP));

        if (tP == -1) {
            System.out.println("FAILURE");
            SubProblemResult result = new SubProblemResult(Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                    null, null, Double.MAX_VALUE, Double.MAX_VALUE, 0);

            return result;
        }

        int tEnd = tP;
        int indexPEnd = indexP;

        int[] sectorsCrossed = new int[nT];

        for (int tt = 0; tt < nT; tt += 1) {
            sectorsCrossed[tt] = -1;
        }

        boolean finished = false;

        while (!finished) {

            int indexPOld = indexP;
            int tOld = tP;

            indexP = predecessorsP[indexPOld][tOld];
            tP = predecessorsT[indexPOld][tOld];

            ArrayList<Integer> sectorsCrossedP = Arc.getSectorsCrossed(graph.getPoints().get(indexP),
                    graph.getPoints().get(indexPOld), graph.getSectors(), tOld - tP);

            for (int tt = 0; tt < tOld - tP; tt += 1) {
                if (tt < sectorsCrossedP.size()) {
                    sectorsCrossed[tP + tt] = sectorsCrossedP.get(tt);
                }

            }

            finished = graph.getPoints().get(indexP).getId().equals(aircraft.getPointDepId());

            pathInversed.add(graph.getPoints().get(indexP));

        }

        SubProblemResult result = new SubProblemResult(costsSP[indexPEnd][tEnd], costs[indexPEnd][tEnd],
                0., 0., 0.,
                sectorsCrossed, pathInversed, 0., 0.,
                pathInversed.get(0).getLevel());

        boolean ok = false;

        // Iterating on solutions if the first ones are in tabu list (avoiding loops)
        // Differencing the levels
        int k0 = 1;
        int km2 = 1;
        int km1 = 1;
        while (!ok) {

            if (aircraft.inTabu(result.getS())) {
                int level = result.getLevel();
                if (level == 0) {
                    k0 += 1;
                } else if (level == -2) {
                    km2 += 1;
                } else if (level == -1) {
                    km1 += 1;
                }
                pathInversed = new ArrayList<>();

                indexP = graph.getIndex(aircraft.getPointArr());
                tP_1 = argmin(costsSP, nPoints, nT, indexP, INF, km1);
                tP_0 = argmin(costsSP, nPoints, nT, indexP + 1, INF, k0);
                tP_2 = argmin(costsSP, nPoints, nT, indexP + 2, INF, km2);

                tP = -1;

                if ((costsSP[indexP + 1][tP_0] <= costsSP[indexP][tP_1])
                        && (costsSP[indexP + 1][tP_0] <= costsSP[indexP + 2][tP_2])) {
                    tP = tP_0;
                    indexP = indexP + 1;

                } else if ((costsSP[indexP][tP_1] <= costsSP[indexP + 1][tP_0])
                        && (costsSP[indexP][tP_1] <= costsSP[indexP + 2][tP_2])) {
                    tP = tP_1;

                } else {
                    tP = tP_2;
                    indexP = indexP + 2;

                }

                pathInversed.add(graph.getPoints().get(indexP));

                if (tP == -1) {
                    System.out.println("FAILURE");
                    return new SubProblemResult(Double.MAX_VALUE, Double.MAX_VALUE,
                            Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                            null, null, Double.MAX_VALUE, Double.MAX_VALUE, 0);

                }

                tEnd = tP;
                indexPEnd = indexP;

                sectorsCrossed = new int[nT];
                for (int tt = 0; tt < nT; tt += 1) {
                    sectorsCrossed[tt] = -1;
                }

                finished = false;
                while (!finished) {

                    int indexPOld = indexP;
                    int tOld = tP;

                    indexP = predecessorsP[indexPOld][tOld];
                    tP = predecessorsT[indexPOld][tOld];

                    ArrayList<Integer> sectorsCrossedP = Arc.getSectorsCrossed(graph.getPoints().get(indexP),
                            graph.getPoints().get(indexPOld), graph.getSectors(), tOld - tP);

                    for (int tt = 0; tt < tOld - tP; tt += 1) {
                        if (tt < sectorsCrossedP.size()) {
                            sectorsCrossed[tP + tt] = sectorsCrossedP.get(tt);
                        }

                    }

                    finished = graph.getPoints().get(indexP).getId().equals(aircraft.getPointDepId());

                    pathInversed.add(graph.getPoints().get(indexP));
                    result = new SubProblemResult(costsSP[indexPEnd][tEnd], costs[indexPEnd][tEnd],
                            0., 0., 0.,
                            sectorsCrossed, pathInversed, 0.,
                            0., pathInversed.get(0).getLevel());

                }

            } else {
                break;
            }
        }

        return result;
    }

    /**
     * Cost of the arc
     * 
     * @param aircraft
     * @param arc
     * @param wd
     * @param ad
     * @param dt
     * @param mass
     * @param time
     * @param costC
     * @param ff
     * @return
     */
    public static double costArc(Flight aircraft, Arc arc, WindData wd, aCCFData ad, int dt,
            int mass, double time, String costC, double ff) {

        if (costC.equals("accf")) {

            return ACCF_CO2 * ff * 60. * time
                    + arc.getACCF();
        }
        if (costC.equals("gwp20")) {

            return ACCF_CO2 * ff * 60. * time
                    * (1. + 2.2 * arc.getPourcent());
        }
        if (costC.equals("gwp100")) {

            return ACCF_CO2 * ff * 60. * time
                    * (1. + 0.63 * arc.getPourcent());
        }

        return ACCF_CO2 * ff * 60. * time;
    }

    /**
     * Cost of the arc in the subproblem (with lambdas)
     * 
     * @param aircraft
     * @param arc
     * @param wd
     * @param ad
     * @param lambdas
     * @param t
     * @param dt
     * @param sectors
     * @param mass
     * @param time
     * @param costC
     * @param ff
     * @return
     */
    public static double[] costArcSP(Flight aircraft, Arc arc, WindData wd, aCCFData ad,
            double[][] lambdas, int t, int dt, ArrayList<Sector> sectors, int mass, double time, String costC,
            double ff) {

        double s = 0;
        int t_index = t;
        int t_index_max = t + dt;

        ArrayList<Integer> sectorsCross = arc.getSectorsCrossed(sectors, dt);
        for (int i = 0; i < sectorsCross.size(); i++) {

            s += lambdas[t_index][sectorsCross.get(i)];
            t_index += 1;

        }
        double[] results = new double[2];
        results[0] = costArc(aircraft, arc, wd, ad, dt, mass, time, costC, ff);
        results[1] = results[0] - s;

        return results;
    }

    /**
     * @param aircraft
     * @param arc
     * @param wd
     * @param DT
     * @return
     */
    public static double[] computeDt(Flight aircraft, Arc arc, WindData wd, double DT) {
        double t = arc.getP1().time(arc.getP2(), aircraft.getAirspeed(), wd, arc);
        double[] results = new double[2];
        results[0] = t;
        results[1] = t / DT;
        return results;
    }

    /**
     * @param aircraft
     * @param p0
     * @param p1
     * @param wd
     * @param DT
     * @return
     */
    public static double computeDt(Flight aircraft, Node p0, Node p1, WindData wd, double DT) {
        return p0.time(p1, aircraft.getAirspeed(), wd) / DT;
    }

    /**
     * Get argmin for solution recovery.
     * 
     * @param tab
     * @param n
     * @param m
     * @param line
     * @param INF
     * @return
     */
    public static int argmin(double[][] tab, int n, int m, int line, double INF) {
        int index = -1;
        double value = INF;
        for (int i = 0; i < m; i++) {

            if (tab[line][i] < value) {
                value = tab[line][i];
                index = i;
            }
        }

        return index;
    }

    /**
     * Get index of the kth smallest value
     * 
     * @param tab
     * @param n
     * @param m
     * @param line
     * @param INF
     * @param k
     * @return
     */
    public static int argmin(double[][] tab, int n, int m, int line, double INF, int k) {
        return getIndexOfIthSmallestValue(tab[line], k);
    }

    public static int getIndexOfIthSmallestValue(double[] array, int i) {
        if (array == null || array.length == 0 || i <= 0 || i > array.length) {
            throw new IllegalArgumentException("Invalid input");
        }
        double[] sortedArray = Arrays.copyOf(array, array.length);
        Arrays.sort(sortedArray);
        return indexOfValue(array, sortedArray[i - 1]);
    }

    private static int indexOfValue(double[] array, double value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return -1;
    }

}
