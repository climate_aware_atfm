package weather;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Class for contrails data.
 */
public class ContrailsData {
    private final String FILE_NAME0;
    private final String FILE_NAMEm1;
    private final String FILE_NAMEm2;

    private HashMap<String, HashMap<String, Double>> accf0;
    private HashMap<String, HashMap<String, Double>> pourcent0;
    private HashMap<String, HashMap<String, Double>> accfm1;
    private HashMap<String, HashMap<String, Double>> pourcentm1;
    private HashMap<String, HashMap<String, Double>> accfm2;
    private HashMap<String, HashMap<String, Double>> pourcentm2;

    /**
     * Constructor extracting data from three files (one for each FL)
     * 
     * @param filename0
     * @param filenamem1
     * @param filenamem2
     */
    public ContrailsData(String filename0, String filenamem1, String filenamem2) {
        this.FILE_NAME0 = filename0;
        this.FILE_NAMEm2 = filenamem2;
        this.FILE_NAMEm1 = filenamem1;

        try {
            fromFile();

        } catch (NumberFormatException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    /**
     * Get the accf value on an arc p1,p2
     * 
     * @param p1
     * @param p2
     * @param level
     * @return
     */
    public double getACCF(String p1, String p2, int level) {

        if (level == 0) {
            return this.accf0.get(p1).get(p2);
        } else if (level == -1) {
            return this.accfm1.get(p1).get(p2);
        } else {
            return this.accfm2.get(p1).get(p2);
        }

    }

    /**
     * Get the part on an arc p1,p2 in contrail areas
     * 
     * @param p1
     * @param p2
     * @param level
     * @return
     */
    public double getPourcent(String p1, String p2, int level) {

        if (level == 0) {
            return this.pourcent0.get(p1).get(p2);
        } else if (level == -1) {
            return this.pourcentm1.get(p1).get(p2);
        } else {
            return this.pourcentm2.get(p1).get(p2);
        }
    }

    /**
     * Extract data from files.
     * 
     * @throws NumberFormatException
     * @throws IOException
     */
    private void fromFile() throws NumberFormatException, IOException {

        this.accf0 = new HashMap<>();
        this.accfm2 = new HashMap<>();
        this.accfm1 = new HashMap<>();
        this.pourcent0 = new HashMap<>();
        this.pourcentm1 = new HashMap<>();
        this.pourcentm2 = new HashMap<>();

        FileReader filereader0 = new FileReader(FILE_NAME0);
        FileReader filereaderm1 = new FileReader(FILE_NAMEm1);
        FileReader filereaderp1 = new FileReader(FILE_NAMEm2);
        BufferedReader br0 = new BufferedReader(filereader0);
        BufferedReader brm1 = new BufferedReader(filereaderm1);
        BufferedReader brp1 = new BufferedReader(filereaderp1);

        String line;

        String lastWord = null;

        while ((line = br0.readLine()) != null) {

            String[] words = line.split(",");

            if (!words[0].equals("p1")) {
                String p1 = words[0];
                String p2 = words[1];
                Double a = Double.valueOf(words[2]);
                Double p = Double.valueOf(words[3]);

                if (!p1.equals(lastWord)) {
                    lastWord = p1;
                    this.accf0.put(p1, new HashMap<>());
                    this.pourcent0.put(p1, new HashMap<>());
                }

                this.accf0.get(p1).put(p2, Math.pow(10, 8) * a); // Avoiding numerical errors with small numbers
                this.pourcent0.get(p1).put(p2, p);

            }

        }

        br0.close();

        lastWord = null;

        while ((line = brm1.readLine()) != null) {

            String[] words = line.split(",");

            if (!words[0].equals("p1")) {
                String p1 = words[0];
                String p2 = words[1];
                Double a = Double.valueOf(words[2]);
                Double p = Double.valueOf(words[3]);

                if (!p1.equals(lastWord)) {
                    lastWord = p1;
                    this.accfm1.put(p1, new HashMap<>());
                    this.pourcentm1.put(p1, new HashMap<>());
                }

                this.accfm1.get(p1).put(p2, Math.pow(10, 8) * a); // Avoiding numerical errors with small numbers
                this.pourcentm1.get(p1).put(p2, p);

            }

        }

        brm1.close();

        lastWord = null;

        while ((line = brp1.readLine()) != null) {

            String[] words = line.split(",");

            if (!words[0].equals("p1")) {
                String p1 = words[0];
                String p2 = words[1];
                Double a = Double.valueOf(words[2]);
                Double p = Double.valueOf(words[3]);

                if (!p1.equals(lastWord)) {
                    lastWord = p1;
                    this.accfm2.put(p1, new HashMap<>());
                    this.pourcentm2.put(p1, new HashMap<>());
                }

                this.accfm2.get(p1).put(p2, Math.pow(10, 8) * a); // Avoiding numerical errors with small numbers
                this.pourcentm2.get(p1).put(p2, p);

            }

        }

        brp1.close();
    }

}
