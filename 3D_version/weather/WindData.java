package weather;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class for wind data. SUPPOSED NOT SUBJECT TO ALTITUDES.
 */
public class WindData {

    private final String FILE_NAME;

    private final double LAT_MAX;
    private final double LAT_MIN;
    private final double LON_MAX;
    private final double LON_MIN;
    private final double DELTA_LON;
    private final double DELTA_LAT;

    private double[][] wU;
    private double[][] wV;

    /**
     * Constructor from a file.
     * 
     * @param filename
     * @param lat_max
     * @param lat_min
     * @param lon_max
     * @param lon_min
     * @param delta_lat
     * @param delta_lon
     */
    public WindData(String filename, double lat_max, double lat_min, double lon_max, double lon_min, double delta_lat,
            double delta_lon) {

        this.LAT_MIN = lat_min;
        this.LAT_MAX = lat_max;
        this.LON_MIN = lon_min;
        this.LON_MAX = lon_max;
        this.DELTA_LAT = delta_lat;
        this.DELTA_LON = delta_lon;
        this.FILE_NAME = filename;

        int n = (int) ((LAT_MAX - LAT_MIN) / DELTA_LAT);
        int m = (int) ((LON_MAX - LON_MIN) / DELTA_LON);
        try {
            getwFromFile(n, m);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get data from a file.
     * 
     * @param n
     * @param m
     * @throws NumberFormatException
     * @throws IOException
     */
    public void getwFromFile(int n, int m) throws NumberFormatException, IOException {
        this.wU = new double[n + 1][m + 1];
        this.wV = new double[n + 1][m + 1];
        FileReader filereader = new FileReader(FILE_NAME);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");

            if (!words[0].equals("Latitude")) {
                double lat = Double.valueOf(words[0]);
                double lon = Double.valueOf(words[1]);
                double wu = Double.valueOf(words[3]);
                double wv = Double.valueOf(words[4]);

                this.wU[getindexLat(lat)][getindexLon(lon)] = msToKts(wu);
                this.wV[getindexLat(lat)][getindexLon(lon)] = msToKts(wv);
            }

        }
        br.close();

    }

    /**
     * Get index of a latitude in the grid.
     * 
     * @param lat
     * @return
     */
    public int getindexLat(double lat) {
        return (int) ((lat - LAT_MIN) / DELTA_LAT);
    }

    /**
     * Get index of a longitude in the grid.
     * 
     * @param lon
     * @return
     */
    public int getindexLon(double lon) {
        return (int) ((lon - LON_MIN) / DELTA_LON);
    }

    /**
     * Interpolate u component of the wind.
     * 
     * @param lon
     * @param lat
     * @return
     */
    public double interpolateWu(double lon, double lat) {

        // Are they points of the grid ?
        boolean latGrid = (((lat / DELTA_LAT) - (int) (lat / DELTA_LAT) - 1) < 0.01);
        boolean lonGrid = (((lon / DELTA_LON) - (int) (lon / DELTA_LON)) < 0.01);

        if (latGrid) {
            if (lonGrid) {
                return this.wU[getindexLat(lat)][getindexLon(lon)];
            } else {
                double lonA = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonB = Math.ceil(lon / DELTA_LON) * DELTA_LON;
                double wUA = this.wU[getindexLat(lat)][getindexLon(lonA)];
                double dAS = lon - lonA;
                double wUB = this.wU[getindexLat(lat)][getindexLon(lonB)];
                double dBS = lonB - lon;
                return (dBS * wUA + dAS * wUB) / (dAS + dBS);
            }
        } else {
            if (lonGrid) {
                double latA = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latB = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;
                double wUA = this.wU[getindexLat(latA)][getindexLon(lon)];
                double dAS = lat - latA;
                double wUB = this.wU[getindexLat(latB)][getindexLon(lon)];
                double dBS = latB - lat;
                return (dBS * wUA + dAS * wUB) / (dAS + dBS);

            } else {
                double latA = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latB = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latC = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;
                double latD = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;

                double lonA = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonD = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonB = Math.ceil(lon / DELTA_LON) * DELTA_LON;
                double lonC = Math.ceil(lon / DELTA_LON) * DELTA_LON;

                double dA = (lat - latA) * (lat - latA) + (lon - lonA) * (lon - lonA);
                double dB = (lat - latB) * (lat - latB) + (lon - lonB) * (lon - lonB);
                double dC = (lat - latC) * (lat - latC) + (lon - lonC) * (lon - lonC);
                double dD = (lat - latD) * (lat - latD) + (lon - lonD) * (lon - lonD);

                double wuA = this.wU[getindexLat(latA)][getindexLon(lonA)];
                double wuB = this.wU[getindexLat(latB)][getindexLon(lonB)];
                double wuC = this.wU[getindexLat(latC)][getindexLon(lonC)];
                double wuD = this.wU[getindexLat(latD)][getindexLon(lonD)];

                return ((1. / dA) * wuA + (1. / dB) * wuB + (1. / dC) * wuC + (1. / dD) * wuD)
                        / ((1. / dA) + (1. / dB) + (1. / dC) + (1. / dD));
            }
        }

    }

    /**
     * Interpolate v-component of the wind.
     * 
     * @param lon
     * @param lat
     * @return
     */
    public double interpolateWv(double lon, double lat) {

        boolean latGrid = (((lat / DELTA_LAT) - (int) (lat / DELTA_LAT) - 1) < 0.01);
        boolean lonGrid = (((lon / DELTA_LON) - (int) (lon / DELTA_LON)) < 0.01);

        if (latGrid) {
            if (lonGrid) {
                return this.wV[getindexLat(lat)][getindexLon(lon)];
            } else {
                double lonA = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonB = Math.ceil(lon / DELTA_LON) * DELTA_LON;
                double wUA = this.wV[getindexLat(lat)][getindexLon(lonA)];
                double dAS = lon - lonA;
                double wUB = this.wV[getindexLat(lat)][getindexLon(lonB)];
                double dBS = lonB - lon;
                return (dBS * wUA + dAS * wUB) / (dAS + dBS);
            }
        } else {
            if (lonGrid) {
                double latA = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latB = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;
                double wUA = this.wV[getindexLat(latA)][getindexLon(lon)];
                double dAS = lat - latA;
                double wUB = this.wV[getindexLat(latB)][getindexLon(lon)];
                double dBS = latB - lat;
                return (dBS * wUA + dAS * wUB) / (dAS + dBS);

            } else {
                double latA = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latB = Math.floor(lat / DELTA_LAT) * DELTA_LAT;
                double latC = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;
                double latD = Math.ceil(lat / DELTA_LAT) * DELTA_LAT;

                double lonA = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonD = Math.floor(lon / DELTA_LON) * DELTA_LON;
                double lonB = Math.ceil(lon / DELTA_LON) * DELTA_LON;
                double lonC = Math.ceil(lon / DELTA_LON) * DELTA_LON;

                double dA = (lat - latA) * (lat - latA) + (lon - lonA) * (lon - lonA);
                double dB = (lat - latB) * (lat - latB) + (lon - lonB) * (lon - lonB);
                double dC = (lat - latC) * (lat - latC) + (lon - lonC) * (lon - lonC);
                double dD = (lat - latD) * (lat - latD) + (lon - lonD) * (lon - lonD);

                double wuA = this.wV[getindexLat(latA)][getindexLon(lonA)];
                double wuB = this.wV[getindexLat(latB)][getindexLon(lonB)];
                double wuC = this.wV[getindexLat(latC)][getindexLon(lonC)];
                double wuD = this.wV[getindexLat(latD)][getindexLon(lonD)];

                return ((1. / dA) * wuA + (1. / dB) * wuB + (1. / dC) * wuC + (1. / dD) * wuD)
                        / ((1. / dA) + (1. / dB) + (1. / dC) + (1. / dD));
            }

        }
    }

    /**
     * Convert m/s to kts.
     * 
     * @param ms
     * @return
     */
    public static double msToKts(double ms) {
        return ms * 1.9438444924406;
    }
}
