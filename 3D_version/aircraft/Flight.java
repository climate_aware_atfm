package aircraft;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import emissions.Emission;
import geometry.Node;

/**
 * Class representing flight objects
 */
public class Flight {

    private double departureTime;
    private String pointDepId;
    private String pointArrId;
    private Node pointDep;
    private Node pointArr;
    private Emission emissions;
    private double airspeed;
    private double mass;
    private int id;
    private ArrayList<String> tabu;
    private HashMap<Node, Boolean> prohibitedNodes;

    /**
     * Object constructor.
     * 
     * @param departureTime
     * @param pointDepId
     * @param pointArrId
     * @param points
     * @param emissions
     * @param airspeed
     * @param mass
     * @param id
     */
    public Flight(double departureTime, String pointDepId, String pointArrId, ArrayList<Node> points,
            Emission emissions, double airspeed, double mass, int id) {
        this.airspeed = airspeed;
        this.pointDepId = pointDepId;
        this.pointArrId = pointArrId;
        this.pointDep = null;
        this.pointArr = null;
        this.mass = mass;
        this.id = id;
        this.emissions = emissions;

        this.departureTime = departureTime;

        // Searching the starting point in the node list
        for (Node p : points) {
            if (p.getId().equals(this.pointDepId)) {
                this.pointDep = p;
                break;
            }
        }

        // Searching the ending point in the node list
        for (Node pp : points) {
            if (pp.getId().equals(this.pointArrId)) {
                this.pointArr = pp;
                break;
            }
        }

        this.tabu = new ArrayList<>();
        double d = this.pointArr.distance(this.pointDep);
        this.prohibitedNodes = new HashMap<>();
        // Some points will never be visited since they are too far from the straight
        // line.
        for (Node n : points) {
            this.prohibitedNodes.put(n, ((n.distance(this.pointArr) + n.distance(this.pointDep) > 1.5 * d)
                    || (n.distance(this.pointArr) > 2 * d) || (n.distance(this.pointDep) > 2 * d)));
        }

    }

    /**
     * @param s
     * @return True if the path represented by the parameter has already been a
     *         candidate for the considered flight.
     */
    public boolean inTabu(String s) {
        return this.tabu.contains(s);

    }

    /**
     * @param n
     * @return True if n is not allowed for this flight beacause too far from the
     *         straight line.
     */
    public boolean isProhibited(Node n) {
        return this.prohibitedNodes.get(n);
    }

    /**
     * Put a new candidate in the tabu list.
     * 
     * @param s
     */
    public void putInTabu(String s) {
        this.tabu.add(s);
    }

    /**
     * Print tabu list for the flight considered.
     */
    public void printtabou() {
        for (String s : this.tabu) {
            System.out.println(s);
        }
    }

    /**
     * @return The departure time (in minutes from the beginning of the simulation).
     */
    public double getDepartureTime() {
        return departureTime;
    }

    /**
     * @return The id of the departure point.
     */
    public String getPointDepId() {
        return pointDepId;
    }

    /**
     * @return The id of the arrival point.
     */
    public String getPointArrId() {
        return pointArrId;
    }

    /**
     * @return The departure point.
     */
    public Node getPointDep() {
        return pointDep;
    }

    /**
     * @return The arrival point.
     */
    public Node getPointArr() {
        return pointArr;
    }

    /**
     * @return The emissions associated to the aircraft used for this flight.
     */
    public Emission getEmissions() {
        return emissions;
    }

    /**
     * @return The airspeed of the aircraft.
     */
    public double getAirspeed() {
        return airspeed;
    }

    /**
     * @return The initial mass.
     */
    public double getMass() {
        return mass;
    }

    /**
     * Extract a list of flights from a data file.
     * 
     * @param filename File from which data are extracted.
     * @param points
     * @return List of flights from the file.
     * @throws IOException
     */
    public static ArrayList<Flight> getFromFile(String filename, ArrayList<Node> points) throws IOException {
        ArrayList<Flight> aircrafts = new ArrayList<>();
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        HashMap<String, Emission> emissions = new HashMap<>();
        int i = 0;
        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");
            if (!words[0].equals("idDep")) {

                String idDep = words[0];
                String idEnd = words[1];
                double airspeed = Double.valueOf(words[2]);
                Emission emission;
                String type = words[3];
                if (emissions.containsKey(type)) {
                    emission = emissions.get(type);
                } else {
                    Emission em = new Emission("emissionsData/" + words[3]);
                    emissions.put(type, em);
                    emission = em;
                }
                double hDep = Double.valueOf(words[4]);
                double mass = Double.valueOf(words[5]);
                aircrafts.add(new Flight(hDep, idDep, idEnd, points, emission, airspeed, mass, i));
                i += 1;

            }

        }
        br.close();
        System.out.println(aircrafts.size());
        return aircrafts;
    }

    /**
     * Extract a list of flights from a data file.
     * 
     * @param filename File from which data are extracted.
     * @param points
     * @param n        Number of flights wanted in the simulation.
     * @return List of flights from the file.
     * @throws IOException
     */
    public static ArrayList<Flight> getFromFile(String filename, ArrayList<Node> points, int n) throws IOException {
        ArrayList<Flight> aircrafts = new ArrayList<>();
        FileReader filereader = new FileReader(filename);
        BufferedReader br = new BufferedReader(filereader);
        String line;
        int i = 0;
        HashMap<String, Emission> emissions = new HashMap<>();
        while ((line = br.readLine()) != null) {

            String[] words = line.split(",");
            if (!words[0].equals("idDep")) {

                String idDep = words[0];
                String idEnd = words[1];
                double airspeed = Double.valueOf(words[2]);
                Emission emission;
                String type = words[3];
                if (emissions.containsKey(type)) {
                    emission = emissions.get(type);
                } else {
                    Emission em = new Emission("emissionsData/" + words[3]);
                    emissions.put(type, em);
                    emission = em;
                }
                double hDep = Double.valueOf(words[4]);
                double mass = Double.valueOf(words[5]);
                aircrafts.add(new Flight(hDep, idDep, idEnd, points, emission, airspeed, mass, i));
                i += 1;
                if (i > n) {
                    break;
                }
            }

        }
        br.close();
        return aircrafts;
    }

    /**
     * @return The id of the flight.
     */
    public int getId() {
        return id;
    }

}
