import random

import geopandas as gpd
import geoplot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from geopy.distance import great_circle
from shapely.geometry import Point, Polygon
from shapely.ops import cascaded_union
from shapely.prepared import prep
import math

COUNTRY_NAME = 'Germany' # Change according to the wanted country
FILE_NAME = "TM_WORLD_BORDERS-0.3.shp" # from https://thematicmapping.org/downloads/world_borders.php
DELTA = 0.5 
N_SECTORS = 17 
MIN_DISTANCE_BETWEEN_TWO_POINTS = 5
NB_POINTS = 200
FILE_NAME_SAVE_POINTS = 'test.csv' # File in which points are saved 
FILE_NAME_SAVE_SECTORS = 'test.txt' # File in which sectors are saved


def get_distance_NM(lon0,lon1,lat0,lat1):
    """Return the distance in nautical miles between two points"""
    return great_circle((lat0,lon0),(lat1,lon1)).meters/1852

def grid_bounds(geom, delta):
    minx, miny, maxx, maxy = geom.bounds
    nx = int((maxx - minx)/delta)
    ny = int((maxy - miny)/delta)
    gx, gy = np.linspace(minx,maxx,nx), np.linspace(miny,maxy,ny)
    grid = []
    for i in range(len(gx)-1):
        for j in range(len(gy)-1):
            poly_ij = Polygon([[gx[i],gy[j]],[gx[i],gy[j+1]],[gx[i+1],gy[j+1]],[gx[i+1],gy[j]]])
            grid.append( poly_ij )
    return grid


def partition(geom, delta):
    """Cut a geometry into squares of delta"""
    prepared_geom = prep(geom)
    grid = list(filter(prepared_geom.intersects, grid_bounds(geom, delta)))
    return grid

def get_sectors(geometry,n_sectors,delta):
    """Groups squares in order to create sectors"""
    grid = partition(geometry.convex_hull,delta)
    tab = None
    nbIterError = 0
    removed1 = False
    removed2 = False
    r = False
    i  = 0
    while (len(grid)>n_sectors):
        np.random.shuffle(grid)
        removed1 = False
        removed2 = False
        try:
            if nbIterError>10 or r:
                nbIterError =0
                g1 = grid[i]
                i = (i+1)%len(grid)
            else:
                g1 = min(grid,key=lambda x : x.area)
        
            if tab !=None:
                g.append(tab)
                tab = None
            grid.remove(g1)
            removed1=True
            l =[g for g in grid if len(set(g.boundary.coords).intersection(g1.boundary.coords))>=2]
            if len(l)==0:
                tab = g1
                nbIterError+=1
                continue
            g2 = min(l,key=lambda x : x.area)
            
            g = cascaded_union([g1,g2])
            if g.geom_type!='Polygon':
                tab = g1
                nbIterError+=1
                continue
            grid.remove(g2)
            removed2=True
            grid.append(g)
            nbIterError =0
            r = False
        except:
            nbIterError +=1
            if removed1:
                grid.append(g1)
            if removed2:
                grid.append(g2)
            continue
    return grid

def get_random_points(geometry,n_points,min_distance):
    """Get n_points random points in the geometry, with a min distance satisfied between points"""
    polygon = geometry.convex_hull
    env = polygon.bounds
    xmin, ymin, xmax, ymax = env
    points = []
    while len(points)<n_points:
        pnt = Point(random.uniform(xmin, xmax), random.uniform(ymin, ymax))
        if polygon.contains(pnt):
            f = True
            for p in points:
                d = get_distance_NM(p.x,pnt.x,p.y,pnt.y)
                if d<min_distance:
                    f = False
                    break
            if f:
                points.append(pnt)
    return points

def main(plot=True,savefile=True):
    data = gpd.read_file(FILE_NAME)
    print("Data loaded.")
    geometry=(data[data['NAME']==COUNTRY_NAME]['geometry'].iloc[0])
    print("Geometry for the country found.")
    points = get_random_points(geometry,NB_POINTS,MIN_DISTANCE_BETWEEN_TWO_POINTS)
    print("Points randomly obtained.")
    sectors = get_sectors(geometry,N_SECTORS,DELTA)
    print("Sectors computed.")
    if plot:
        f, ax = plt.subplots(1, 1, figsize=(12, 9))
        geoplot.polyplot(data[data['NAME']==COUNTRY_NAME], edgecolor='darkgrey', facecolor='lightgrey', linewidth=.3,
            figsize=(12, 8),ax=ax)
        gpd.GeoSeries(sectors).boundary.plot(ax=ax)
        gdf_points = gpd.GeoDataFrame(geometry=points)
        geoplot.pointplot(gdf_points,ax=ax)
        plt.show()
    if savefile:
        # Saving points 
        data_points = []
        for ip,p in enumerate(points):
            d = {}
            d['name']=ip
            d['lat']=p.y
            d['lon']=p.x
            for js,s in enumerate(sectors):
                if s.contains(p):
                    d['sector']=js
                    break
            data_points.append(d)
        df = pd.DataFrame.from_records(data_points)
        df.to_csv(FILE_NAME_SAVE_POINTS,index=False,index_label=None)
        print('Points saved in {}.'.format(FILE_NAME_SAVE_POINTS))
        # Saving sectors
        with open(FILE_NAME_SAVE_SECTORS,"w") as file_sectors:
            for s in sectors:
                xx, yy = s.exterior.coords.xy
                x = list(xx)
                y = list(yy)
                line = ""
                for xi,yi in zip(x,y):
                    sign_x = "E" if xi>0 else "W"
                    int_x = math.floor(xi)
                    min_x = math.floor(60*(xi-int_x))
                    sec_x = math.floor(3600*(xi-int_x-min_x/60))
                    sx = "{:03}°{:02}'{:02}\"{}".format(int_x,min_x,sec_x,sign_x)

                    
                    
                    sign_y = "N" if xi>0 else "S"
                    int_y = math.floor(yi)
                    min_y = math.floor(60*(yi-int_y))
                    sec_y = math.floor(3600*(yi-int_y-min_y/60))
                    sy = "{:02}°{:02}'{:02}\"{}".format(int_y,min_y,sec_y,sign_y)

                    line += sy + " , "+sx + " - "
                # remove the last " - "
                line = line[:-3]
                line += '\n'
                file_sectors.write(line)
        print('Sectors saved in {}.'.format(FILE_NAME_SAVE_SECTORS))
                

if __name__=="__main__":
    main(plot=False)