import pandas as pd
from datetime import datetime
import numpy as np
from geopy.distance import great_circle

# Data for true airspeed 
FN_TAS_MTOW = 'data_tas_mtow.csv'
DF_TAS_MTOW = pd.read_csv(FN_TAS_MTOW)

CONVERSION = {'A20N':'A320','A21N':'A321','A306':'A320','A318':'A319','A346':'A343','A359':'A343',
              'B38M':'B738','B734':'B737','B736':'B737','B748':'B744','B753':'B752','B762':'B752',
              'B763':'B752','B764':'B752','B772':'B77W','B77L':'B77W','BCS1':'E190','BCS3':'E190',
              'BE40':'E190','C25A':'E190','C25B':'E190','C510':'E190', 'C525':'E190', 'C56X':'E190',
                'C68A':'E190', 'CL35':'E190', 'CL60':'E190', 'CRJ2':'E190', 'CRJ7':'E190', 'CRJX':'E190', 
                'E145':'E190', 'E170':'E190', 'E195':'E190', 'E35L':'E190', 'E50P':'E190', 'E55P':'E190', 
                'E75L':'E190', 'E75S':'E190', 'F100':'E190', 'F2TH':'E190', 'F900':'E190', 'FA10':'E190', 
                'FA7X':'E190', 'GLEX':'E190', 'GLF5':'E190', 'H25B':'E190', 'LJ60':'E190', 'LJ75':'E190',
                  'MD11':'E190', 'PRM1':'E190', 'RJ85':'E190'
}

def get_distance_NM(lon0,lon1,lat0,lat1):
    return great_circle((lat0,lon0),(lat1,lon1)).meters/1852

def distance_cart(lon0,lon1,lat0,lat1):
    return (lon0-lon1)*(lon0-lon1)+(lat0-lat1)*(lat0-lat1)


def process(fn_points, fn_flights,lon_min,lon_max,lat_min,lat_max,str_date_min,str_date_max,fl_min,fl_max,min_distance_flown,fn_airspace,coeff_mtow,fn_result):
    df_flights = pd.read_csv(fn_flights)
    df_points = pd.read_csv(fn_points)

    df_flights['FILED OFF BLOCK TIME']=df_flights['FILED OFF BLOCK TIME'].map(lambda x : datetime.strptime(x,"%d-%m-%Y %H:%M:%S"))
    df_flights['FILED ARRIVAL TIME']=df_flights['FILED ARRIVAL TIME'].map(lambda x : datetime.strptime(x,"%d-%m-%Y %H:%M:%S"))
    df_flights['ACTUAL OFF BLOCK TIME']=df_flights['ACTUAL OFF BLOCK TIME'].map(lambda x : datetime.strptime(x,"%d-%m-%Y %H:%M:%S"))
    df_flights['ACTUAL ARRIVAL TIME']=df_flights['ACTUAL ARRIVAL TIME'].map(lambda x : datetime.strptime(x,"%d-%m-%Y %H:%M:%S"))


    # Filtering data on date and lon/lat min/max 
    df_points = df_points[df_points['Longitude']>=lon_min]
    df_points = df_points[df_points['Longitude']<=lon_max]
    df_points = df_points[df_points['Latitude']>=lat_min]
    df_points = df_points[df_points['Latitude']<=lat_max]

    df_points['Time Over']=df_points['Time Over'].map(lambda x : datetime.strptime(x,"%d-%m-%Y %H:%M:%S"))

    print(df_points['Time Over'])

    d_min = datetime.strptime(str_date_min,"%d-%m-%Y %H:%M:%S") #"05-03-2019 10:00:00"
    d_max = datetime.strptime(str_date_max,"%d-%m-%Y %H:%M:%S")

    df_points = df_points[df_points['Time Over']>=d_min]
    df_points = df_points[df_points['Time Over']<d_max]


    groups = df_points.groupby('ECTRL ID')
    ids = np.unique(df_points['ECTRL ID'])


    print("Number of ids after filtering on date and lat/lon min/max : {}".format(ids.size))

    # Filtering on chosen altitudes
    df_points = df_points[df_points['Flight Level']>=fl_min]
    df_points = df_points[df_points['Flight Level']<=fl_max]

    groups = df_points.groupby('ECTRL ID')
    ids = np.unique(df_points['ECTRL ID'])

    print("Number of ids after filtering on altitude : {}".format(ids.size))

    # Filtering on distance flown 
    ids_keep = []
    for n,g in groups:
        d = (get_distance_NM(g.iloc[0].Longitude,g.iloc[-1].Longitude,g.iloc[0].Latitude,g.iloc[-1].Latitude))
        if d>min_distance_flown:
            ids_keep.append(n)

    print("Number of ids after filtering on minimum distance flown : {}".format(len(ids_keep)))

    # Computing data from this new list of ids
    
    df_waypoints = pd.read_csv(fn_airspace)
    df_points = df_points[df_points['ECTRL ID'].isin(ids_keep)]
    groups = df_points.groupby("ECTRL ID")
    data_flights = []
    for n,g in groups:
        type_ac=df_flights[df_flights['ECTRL ID']==n].iloc[0]['AC Type']
        if type_ac in CONVERSION:
            type_ac = CONVERSION[type_ac]
        #Retriving an error 
        if type_ac in ['SW4','A3ST']:
            continue
        d = {}
        lon_dep,lat_dep = g.iloc[0].Longitude,g.iloc[0].Latitude
        lon_end,lat_end = g.iloc[-1].Longitude,g.iloc[-1].Latitude
        d_dep = -1
        d_end = -1
        lat_dep_,lon_dep_ = 0,0
        lat_end_,lon_end_ = 0,0
        id_dep = None
        id_end = None 
        for id,lon,lat in zip(df_waypoints['name'],df_waypoints['lon'],df_waypoints['lat']):
            dist_dep = get_distance_NM(lon_dep,lon,lat_dep,lat)
            dist_end = get_distance_NM(lon_end,lon,lat_end,lat)
            if dist_dep<d_dep or d_dep ==-1:
                id_dep = id
                d_dep = dist_dep
                lat_dep_,lon_dep_ = lat,lon
            if dist_end<d_end or d_end ==-1:
                id_end = id
                d_end = dist_end
                lat_end_,lon_end_ = lat,lon
        if (get_distance_NM(lon_dep_,lon_end_,lat_dep_,lat_end_)>min_distance_flown):
            d['idDep']=id_dep
            d['idEnd']=id_end
            print(type_ac)
            d['airspeed']=DF_TAS_MTOW[DF_TAS_MTOW['aircraft']==type_ac].iloc[0]['tas']
            d['fileEmission']='{}.csv'.format(type_ac)
            d['hDep']=g.iloc[0]['Time Over'].minute
            d['mass0']=int(coeff_mtow*DF_TAS_MTOW[DF_TAS_MTOW['aircraft']==type_ac].iloc[0]['mtow'])
            data_flights.append(d)
    print("Number of ids at the end of the process : {}".format(len(data_flights)))
    df_final = pd.DataFrame.from_records(data_flights)
    df_final.to_csv(fn_result,index=False,index_label=None)
    print('File created.')


if __name__=='__main__':
    fn_points = "Flight_Points_Filed_20190301_20190331.csv.gz" #FILENAME FOR EUROCONTROL R&D POINTS DATA 
    fn_flights = "Flights_20190301_20190331.csv.gz" #FILENAME FOR EUROCONTROL R&D FLIGHTS DATA (SAME MONTH)
    lon_min,lon_max = -5,8.6 
    lat_min,lat_max = 41,51.5 # GEOGRAPHICAL AREA DEFINITION
    str_date_min = "05-03-2019 10:00:00"
    str_date_max = "05-03-2019 11:00:00" # HERE AIRCRAFT ARE CONSIDERED IF THEY ENTER THE AIRSPACE BETWEEN THE TWO DATES
    fl_min,fl_max  = 300,400 # CRUISE PHASE DEFINITION 
    min_distance_flown = 150 # MINIMUM DISTANCE BETWEEN THE STARTING AND THE ENDING POINTS 
    fn_airspace = "../data/points_france.csv" # FILENAME TO FIND POINTS
    coeff_mtow = 0.8 # COEFFICIENT FOR INITIAL MASS FUNCTION OF MTOW
    fn_result = "flights.csv" # FILENAME FOR RESULTS SAVING
    process(fn_points, fn_flights,lon_min,lon_max,lat_min,lat_max,str_date_min,str_date_max,fl_min,fl_max,min_distance_flown,fn_airspace,coeff_mtow,fn_result)