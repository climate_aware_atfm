This repository contains python code for random instances. 

# process_traffic_eurcontrol.py 

### Required packages 
pandas, datetime, numpy, geopy

### Required data 
Flight and points files from: https://www.eurocontrol.int/dashboard/rnd-data-archive. 

### Input parameters 
- _fn_points_ : Filename of points file from Eurocontrol R&D data
- _fn_flights_: Filename of flights file from Eurocontrol R&D data 
- _lon\_min,lon\_max,lat\_min,lat\_max_: minimum and maximum longitudes and latitudes for the considered area
- _str\_date\_min,str\_date\_max_ : minimum and maximum dates for time period considered (under the following pattern: "05-03-2019 10:00:00")
 - _fl\_min,fl\_max_: minimum and maximum flight levels for cruise consideration (for instance 250 and 450)
- _min\_distance\_flown_: minimum distance between the starting and the ending points (for long enough flights consideration) 
- _fn\_airspace_: filename where to find waypoints definition
- _coeff\_mtow_: coefficient for mass consideration, between 0 and 1
- _fn\_result_: filename in which results are saved

The file _data\_tas\_mtow.csv_ is necessary for getting mass and true airspeed. It has been extracted from OpenAP (https://openap.dev/) and can be completed.

# create_random_airspace.py

This programm creates a random sectorization from a grid of the space and sample the space into a given number of random waypoints. 

### Required packages 
random,geopandas,geoplot,matplotlib,numpy,pandas,geopy,shapely,math

### Required data 
Data for borders are required and can be downloaded from: https://thematicmapping.org/downloads/world_borders.php 


### Input parameters
- _COUNTRY\_NAME_: the name of the country considered (according to data)
- _FILE\_NAME_: filename in which borders data can be found 
- _DELTA_: size of squares in the initial grid for sectors consideration
- _N\_SECTORS_: wanted number of sectors
- _MIN\_DISTANCE\_BETWEEN\_TWO\_POINTS_: minimum distance (in NM) between two sampled points
- _NB\_POINTS_: wanted number of waypoints
- _FILE\_NAME\_SAVE\_POINTS_: csv file in which points are saved
- _FILE\_NAME\_SAVE\_SECTORS_: txt file in which sectors are saved

Bugs can occur with countries that have far oversea parts. Parameters should be adapted to the country considered, and in particular, to its area.