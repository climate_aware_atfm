# Data for French airspace

This repository contains data for the French airspace. They are built from the website of French aeronautical information service: https://www.sia.aviation-civile.gouv.fr/. They are not made for operationnal use and are different from operationnal data since they are only made for optimization process evaluation purpose. 

- _points\_france.csv_

csv file with all considered waypoints. 
The structure is as follows : _name,lat,lon,sector_. lat and lon are latitudes and longitudes in degrees. sector is the sector of the given point. 

- _sectors\_france.txt_:

txt file where a line is a sector. 
Each line follows this pattern : _lat , lon - lat, lon-..._ where lat is the latitude in degrees, minutes and seconds (for instance 43°55'10"N) and lon is the longitude in degrees, minutes and seconds (for instance 001°28'20"E).